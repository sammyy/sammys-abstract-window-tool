package resources.exceptions;

/**
 * Indicating a {@link Line.Class} or  {@link LineMath.Class}
 * has a perpendicular line which doesn't follow the original ways
 * of treating a line.
 * @author Sammyy
 *
 */
public class PerpendicularLineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8544121785546127408L;

}
