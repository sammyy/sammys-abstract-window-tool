package resources.exceptions;

/**
 * Means that two Lines were compared to eachother for a point
 * of intersection. Intersection occurred, but on multiple points.
 * @author Sammyy
 *
 */
public class MultipleIntersectionsPointsForLineException extends Exception {

}
