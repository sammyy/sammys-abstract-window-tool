package resources.data.containers;

import entities.foundation.Entity;

/**
 * A pair of type T and a key(int).
 * @author Sammyy
 *
 * @param <T>
 */
public class EntityKey<T extends Entity> {
	
	private T entity;
	private int key;
	
	public EntityKey(T entity, int key) {
		this.entity = entity;
		this.key = key;
	}
	
	public T getEntity() {
		return entity;
	}

	public int getKey() {
		return key;
	}


}
