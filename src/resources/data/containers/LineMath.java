package resources.data.containers;

import entities.foundation.Line;
import resources.exceptions.PerpendicularLineException;

/**
 * Contains mathematical values from the equation of a Line.
 * If a Line do not conform to the the usual expression then
 * {@link #isPerp()} will return true. Meaning the is no incline
 * or M value. This is a special case.
 * @author Sammyy
 *
 */
public class LineMath {
	
	private Location rightPoint;
	private Location leftPoint;
	private Location diff;
	private Float incline;
	private Integer m;
	
	/**
	 * 
	 * @return
	 */
	public boolean isPerp() {
		return isPerp;
	}

	private boolean isPerp = false;
	
	public LineMath(Line line) {
		this.initValues(line);
	}
	
	public void initValues(Line line) {
		rightPoint = Location.getBiggestX(line.getA(), line.getB()).add(line.getLocation());
        leftPoint = Location.getSmallestX(line.getA(), line.getB()).add(line.getLocation());
        diff = rightPoint.subtract(leftPoint);
      
        this.initPerpendicular(line);
        incline = diff.getY() / (float)diff.getX();
        m = (int) Math.round(rightPoint.getY() - rightPoint.getX() * incline);
	}
	
	private void initPerpendicular(Line line) {
		if(diff.getX() == 0) {
			rightPoint = Location.getBiggestY(line.getA(), line.getB()).add(line.getLocation());
			leftPoint = Location.getSmallestY(line.getA(), line.getB()).add(line.getLocation());
			diff = rightPoint.subtract(leftPoint);
			incline = null;
			m = null;
			isPerp = true;
		}
	}
	
	public Float getY(float x) {
		if(this.isPerp) return null;
		return x * this.getIncline() + this.getM();
	}

	/**
	 * In case of being a perpendicular line(two points same X, but differren Ys)
	 * this will return edge Location of line with largest Y. 
	 * Normal line returns edge Location of line with largest X;
	 * @return
	 */
	public Location getRightPoint() {
		return rightPoint.getCopy();
	}
	
	/**
	 * In case of being a perpendicular line(two points same X, but differren Ys)
	 * this will return edge Location of line with smallest Y. 
	 * Normal line returns edge Location of line with smallest X;
	 * @return
	 */
	public Location getLeftPoint() {
		return leftPoint.getCopy();
	}

	public Location getDiff() {
		return diff.getCopy();
	}

	public float getIncline() {
		return incline;
	}

	public int getM() {
		return m;
	}

}
