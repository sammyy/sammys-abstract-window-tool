/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import utilities.abstracts.Resource;

/**
 * Used for retrieving fundamental data about client's
 * image for each server-side object.(Later sent back)
 * @author Sammyy
 */
public class EntityClientInfo extends Resource<String>{
    
    public static final String ID = "ID";
    public static final String CLASS = "CLASS";
    public static final String NAME = "NAME";
    public static final String DESCRIPTION = "DESC";
    public static final String MAX_WIDTH= "WIDTH";
    public static final String MAX_HEIGHT = "HEIGHT";
    /**
     * Relative to client's path.
     */
    public static final String IMG_PATH = "IMG_PATH";
    
       public EntityClientInfo(String id, String className, String name, String description,
              String maxWidth, String maxHeight, String imgPath){
        this.addProperty(ID, id);
        this.addProperty(CLASS, className);
        this.addProperty(NAME, name);
        this.addProperty(DESCRIPTION, description);
        this.addProperty(MAX_WIDTH, maxWidth);
        this.addProperty(MAX_HEIGHT, maxHeight);
        this.addProperty(IMG_PATH, imgPath);
    }
       
    public String getId(){
        return this.getProperty(ID);
    }   
    
    public String getClassName(){
        return this.getProperty(CLASS);
    }   
     
    public String getName(){
        return this.getProperty(NAME);
    }   
      
    public String getDescription(){
        return this.getProperty(DESCRIPTION);
    }   
       
    public String getMaxWidth(){
        return this.getProperty(MAX_WIDTH);
    }   
        
    public String getMaxHeight(){
        return this.getProperty(MAX_HEIGHT);
    }   
         
    public String getImgPath(){
        return this.getProperty(IMG_PATH);
    }   
    
    public Dimensions getDimensions() {
    	return new Dimensions(this.getMaxWidth(), this.getMaxHeight());
    }
    
    @Override
    public String getProperty(String key){
        return super.getProperty(key);
    }
    
}
