/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import entities.foundation.Entity;

/**
 *
 * @author Sammyy
 */
public enum Direction {  
   // UP, UP_RIGHT,UP_LEFT,
   //  LEFT, RIGHT, 
   // DOWN, DOWN_LEFT, DOWN_RIGHT;
	FRONT, FRONTLEFT, FRONTRIGHT,
	LEFT, RIGHT,
	BACK, BACKRIGHT, BACKLEFT;
	
	/**
	 * Camelcase as: camelCase, frontRight
	 * For serveruse
	 * @param dir
	 * @return
	 */
	public static String getStringifiedDirection(Direction dir) {
		 switch(dir){
        case BACK: return "back";
        case FRONT: return "front";
        case RIGHT: return "right";
        case LEFT: return "left";
        case BACKRIGHT: return  "backRight";
        case BACKLEFT: return "backLeft";
        case FRONTRIGHT: return "frontRight";
        case FRONTLEFT: return "frontLeft";
        default: return "frontRight";
        }
		 //TODO return null;
	}
	
	/**
	 * {@link Direction#getStringifiedDirection(Direction)}
	 */
	public String stringified() {
		return Direction.getStringifiedDirection(this);
	}
	
	/**
	 * Perpendicular is considered to be: FRONT, BACK, LEFT & RIGHT.
	 * @param dir
	 * @return
	 */
	public boolean isPerpendicular() {
		Direction dir = this;
		return (dir == FRONT || dir == BACK || dir == LEFT || dir == RIGHT);
	}
	
	/**
	 * Whether the direction is heading to the left
	 * @return
	 */
	public boolean isDirectionLeftSided() {
		switch(this){
			case LEFT: return true;
			case FRONTLEFT: return true;
			case BACKLEFT: return true;
			default: return false;
		}
	}
	
	/**
	 * Whether the direction is heading to the right
	 * @return
	 */
	public boolean isDirectionRightSided() {
		switch(this){
			case RIGHT: return true;
			case FRONTRIGHT: return true;
			case BACKRIGHT: return true;
			default: return false;
		}
	}
	
	/**
	 * 
	 * @param directionValue stringified version of Direction 
	 * @return Direction or null
	 */
	public static Direction getDirection(String directionValue) {
		if(directionValue.equalsIgnoreCase("back")) return Direction.BACK;
		if(directionValue.equalsIgnoreCase("backLeft")) return Direction.BACKLEFT;
		if(directionValue.equalsIgnoreCase("backRight")) return Direction.BACKRIGHT;
		if(directionValue.equalsIgnoreCase("front")) return Direction.FRONT;
		if(directionValue.equalsIgnoreCase("frontLeft")) return Direction.FRONTLEFT;
		if(directionValue.equalsIgnoreCase("frontRight")) return Direction.FRONTRIGHT;
		if(directionValue.equalsIgnoreCase("left")) return Direction.LEFT;
		if(directionValue.equalsIgnoreCase("right")) return Direction.RIGHT;
		return null;
	}
	
	/**
     * Get Direction between "from" and "to". So that The direction points towards "to".
     * @param from Direction pointing from this location
     * @param to Direction pointing to this location/as close as possible.
     * @return Direction
     */
    public static Direction getDirection(Location from, Location to){
    	//Where from is situated in relation to "to"
        boolean below = false;
        boolean right = false;
        boolean left = false;
        boolean above = false;
        
        if(from.getY() > to.getY()){
            below = true;
        } else if(from.getY() < to.getY()){
            above = true;
        }
        
        if(from.getX() > to.getX()){
            right = true;
        } else if(from .getX() < to.getX()){
            left = true;
        }
      
        if(below && !left && !right){
            return Direction.BACK;
        } else if(above && !left && !right){
            return Direction.FRONT;
        } else if(right && !below && !above){
            return Direction.LEFT;
        } else if(left && !below && !above){
            return Direction.RIGHT;
        } else if(below && left) {
        	return Direction.BACKRIGHT;
        } else if(below && right) {
        	return Direction.BACKLEFT;
        } else if(above && left) {
        	return Direction.FRONTRIGHT;
        } else if(above && right) {
        	return Direction.FRONTLEFT;
        } 
        return null;
    }
	
}
