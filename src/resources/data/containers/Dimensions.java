/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import java.util.Collections;
import java.util.Map;
import utilities.abstracts.Resource;

/**
 * Used to set dimensions for text or an
 * image.
 * @author Sammyy
 */
public class Dimensions extends Resource<String>{
    
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    //Font related below
    public static final String NAME = "name";
    public static final String SIZE = "size";
    public static final String COLOR = "color";

    /**
     * Set image dimensions.
     * @param width
     * @param height 
     */
    public Dimensions(String width, String height){
        setImageDimensions(width, height);
    }
    /**
     * Set text dimensions.
     * @param maxWidth
     * @param fontName
     * @param fontSize 
     */
    public Dimensions(String maxWidth, String fontName, String fontSize, String fontColor){
        setTextDimensions(maxWidth, fontName, fontSize, fontColor);
    }
    
    public Dimensions(String maxWidth, String fontName, int fontSize, String fontColor){
        setTextDimensions(maxWidth, fontName, fontSize, fontColor);
    }
    
    public Dimensions(int maxWidth, String fontName, int fontSize, String fontColor){
        setTextDimensions(maxWidth, fontName, fontSize, fontColor);
    }
    
    public Dimensions(int maxWidth, String fontName, String fontSize, String fontColor){
        setTextDimensions(maxWidth, fontName, fontSize, fontColor);
    }
    
    public Dimensions(Map<String, String> props){
        super(props);
    }
    
    public Dimensions(int width, int height) {
		this.setImageDimensions(width + "", height + "");
	}
	public void clearDimensions(){
        this.clearProperties();
    }
    
    public void setImageDimensions(String width, String height){
        this.clearDimensions();
        this.addProperty(WIDTH, width);
        this.addProperty(HEIGHT, height);
    }
    
    public void setTextDimensions(String maxWidth, String fontName, String fontSize, String fontColor){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth);
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize);
        this.addProperty(COLOR, fontColor);
    }
    
    public void setTextDimensions(String maxWidth, String fontName, int fontSize, String fontColor){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth);
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize + "");
        this.addProperty(COLOR, fontColor);
    }
    
    /**
     * Default color is black.
     * @param maxWidth
     * @param fontName
     * @param fontSize
     */
    public void setTextDimensions(String maxWidth, String fontName, String fontSize){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth);
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize);
        this.addProperty(COLOR, "#000000");
    }
    
    /**
     * Default color is black.
     * @param maxWidth
     * @param fontName
     * @param fontSize
     */
    public void setTextDimensions(String maxWidth, String fontName, int fontSize){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth);
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize + "");
        this.addProperty(COLOR, "#000000");
    }
    
    public void setTextDimensions(int maxWidth, String fontName, String fontSize, String fontColor){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth + "");
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize);
        this.addProperty(COLOR, fontColor);
    }
    
    public void setTextDimensions(int maxWidth, String fontName, int fontSize, String fontColor){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth + "");
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize + "");
        this.addProperty(COLOR, fontColor);
    }
    
    /**
     * Default color is black.
     * @param maxWidth
     * @param fontName
     * @param fontSize
     */
    public void setTextDimensions(int maxWidth, String fontName, String fontSize){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth + "");
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize);
        this.addProperty(COLOR, "#000000");
    }
    
    /**
     * Default color is black.
     * @param maxWidth
     * @param fontName
     * @param fontSize
     */
    public void setTextDimensions(int maxWidth, String fontName, int fontSize){
        this.clearDimensions();
        this.addProperty(WIDTH, maxWidth + "");
        this.addProperty(NAME, fontName);
        this.addProperty(SIZE, fontSize + "");
        this.addProperty(COLOR, "#000000");
    }
    
    /**
     * Copy of the object
     * @return
     */
    public Dimensions getImmutableDimensions(){
        if(this.getFontSize() != null){
            return new Dimensions(this.getWidth()+"",this.getFontName(), 
                this.getFontSize(), this.getFontColor());
        } else {
            return new Dimensions(this.getWidth()+"",this.getHeightAsString());
        }
    }
    
    public String getFontColor() {
		return this.getProperty(COLOR);
	}
    
	@Override
    public String getProperty(String key){
        return super.getProperty(key);
    } 
    
    public String getWidthAsString(){
        return this.getProperty(WIDTH);
    }
    
    /**
     * If no width set(eg: Text entity non clickable no specific width)
     * then width is 1.
     * @return
     */
    public Integer getWidth(){
    	String value = this.getProperty(WIDTH);
        return (value == null || value.isEmpty()) ? 1 : Integer.valueOf(this.getProperty(WIDTH));
    }
    
     /**
     * Retrieves approximated height for font if this is
     * text dimensions.
     * @return 
     */
    public String getHeightAsString(){
        if(this.getProperty(HEIGHT) != null){
            return this.getProperty(HEIGHT);
        } else {
            return this.getProperty(SIZE);//Font height approximation
        }
    }
    
    /**
     * Retrieves size of font if this is
     * text dimensions.
     * @return 
     */
    public Integer getHeight(){
        if(this.getProperty(HEIGHT) != null){
            return Integer.valueOf(this.getProperty(HEIGHT));
        } else {
            return Integer.valueOf(this.getProperty(SIZE));//Font height approximation
        }
    }
    
    public String getFontName(){
        return this.getProperty(NAME);
    }

    public Integer getFontSize(){
        String fontSize = this.getProperty(SIZE);
        if(fontSize != null) {
        	return Integer.parseInt(fontSize);
        }
        return null;
    }
    
}
