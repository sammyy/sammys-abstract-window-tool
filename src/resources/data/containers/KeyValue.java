package resources.data.containers;

import java.util.Map.Entry;

public class KeyValue<T, T2> implements Entry<T, T2>{
	
	private T key = null;
	private T2 value = null;
	
	public KeyValue(T key, T2 value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public T getKey() {
		return key;
	}

	@Override
	public T2 getValue() {
		return value;
	}

	@Override
	public T2 setValue(T2 arg0) {
		this.value = arg0;
		return this.value;
	}

}
