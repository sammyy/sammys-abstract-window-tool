/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import resources.dataHandlers.KeyInputHandler;
import java.util.Map;
import utilities.abstracts.Resource;

/**
 * An object for storing Controller's input from
 * the website connecting Huppi client and server.
 * @author Sammyy
 */
public class WebControllerInput extends Resource<Object>{

    public static final String MOUSE_UP_LOCATION = "mouseUpLocation";
    public static final String MOUSE_DOWN_LOCATION = "mouseDownLocation";
    public static final String LEFT_MOUSE_CLICK = "leftMouseClick";
    public static final String MOUSE_MOVE_LOCATION = "mouseMoveLocation";
    public static final String PRESSED_KEY_CHAR = "pressedKeyChar";
    
    @Override
    public void addProperty(String key, Object value){
        super.addProperty(key, value);
    }
    @Override
    public Object getProperty(String key){
        return super.getProperty(key);
    }
    
    @Override
    public Map<String, Object> getProperties(){
        return super.getProperties();
    }
    
    @Override
    public void clearProperties(){
        super.clearProperties();
    }
    
    /**
     * 
     * @return 
     */
    public Location getUpMouseLocation(){
        return (Location) this.getProperty(MOUSE_UP_LOCATION);
    }   
    
    public Location getDownMouseLocation(){
        return (Location) this.getProperty(MOUSE_DOWN_LOCATION);
    }   
    
    public Location getMoveMouseLocation(){
        return (Location) this.getProperty(MOUSE_MOVE_LOCATION);
    }   
    
 
     public KeyInputHandler getPressedKeyChars(){
        return (KeyInputHandler) this.getProperty(PRESSED_KEY_CHAR);
    }  
     
     public Boolean getLeftMouseClick(){
         return (Boolean) this.getProperty(LEFT_MOUSE_CLICK);
     }   
    
    
}
