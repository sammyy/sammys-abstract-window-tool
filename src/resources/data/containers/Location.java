/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import java.util.ArrayList;
import java.util.List;

import utilities.abstracts.Resource;

/**
 *
 * @author Sammyy
 */
public class Location extends Resource<Integer>{
    
    public static final String X = "x";
    public static final String Y = "y";
    private int x = 0;
    private int y = 0;
    
    public Location(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public Location getCopy(){
        return new Location(this.x, this.y);
    }
    
    @Override
    public String toString(){
        return "x: "+this.getX()+" y: "+this.getY();
    }
    /**
     * Subtract
     * @param compare
     * @return 
     */
    public Location subtract(Location compare){
        return new Location(this.x-compare.getX(), this.y-compare.getY());
    }
    
    public Location add(Location add){
        return new Location(this.getX()+add.getX(), this.getY()+add.getY());
    }
    
    public Location add(int x, int y) {
    	return new Location(this.getX()+x, this.getY()+y);
    }
    
    public static Location getBiggestY(Location a, Location b){
        if(a.getY() >= b.getY()){
            return a;
        }
        return b;
    }
    
    public static Location getBiggestX(Location a, Location b){
        if(a.getX() >= b.getX()){
            return a;
        }
        return b;
    }
    
     public static Location getSmallestY(Location a, Location b){
        if(a.getY() >= b.getY()){
            return b;
        }
        return a;
    }
    
    public static Location getSmallestX(Location a, Location b){
        if(a.getX() >= b.getX()){
            return b;
        }
        return a;
    }
    /**
     * Useful for getting the location to begin paint something.
     * @param a
     * @param b
     * @return 
     */
    public static Location getLeftUpperCorner(Location a, Location b){
        int x = Location.getSmallestX(a, b).getX();
        int y = Location.getSmallestY(a, b).getY();
        return new Location(x, y);
    }
        /**
     * 
     * @param a
     * @param b
     * @return 
     */
    public static Location getRightLowerCorner(Location a, Location b){
        int x = Location.getBiggestX(a, b).getX();
        int y = Location.getBiggestY(a, b).getY();
        return new Location(x, y);
    }
    
    /**
     * Get the direction between two locations
     * @param b Direction towards this location.
     * @return Direction
     */
    public Direction getDirection(Location b){
        Location subtracted = b.subtract(this);
        
        if(subtracted.getX() < 0 && subtracted.getY() < 0){
            return Direction.BACKLEFT;
        }
        
        if(subtracted.getX() < 0 && subtracted.getY() > 0){
            return Direction.FRONTLEFT;
        }
        
        if(subtracted.getX() > 0 && subtracted.getY() < 0){
            return Direction.BACKRIGHT;
        }
        
        if(subtracted.getX() > 0 && subtracted.getY() > 0){
            return Direction.FRONTRIGHT;
        }
        
        if(subtracted.getX() == 0 && subtracted.getY() < 0){
            return Direction.BACK;
        }
        
        if(subtracted.getX() == 0 && subtracted.getY() > 0){
            return Direction.FRONT;
        }
        
        if(subtracted.getX() < 0 && subtracted.getY() == 0){
            return Direction.LEFT;
        }
        
        if(subtracted.getX() > 0 && subtracted.getY() == 0){
            return Direction.RIGHT;
        }
        
        return null;
    }
    
    /**
     * Used to get a location towards a direction with a specific 
     * distance with respect to an origin location.
     * @param origin The location which is the origin for calculatin a new
     * location in desired direction.
     * @param dir The direction the location will point towards when 
     * subtracted from origin
     * @param distanceXdiagonal The distance from origin when direction is diagonal
     * @param distanceYdiagonal The distance from origin when direction is diagonal
     * @param distanceX The distance from origin in x axis when perpendicular
     * @param distanceY The distance from origin in y axis
     * @return Location
     */
    public static Location getLocationInDirection(
    		Location origin, Direction dir, 
            int distanceXdiagonal, int distanceYdiagonal, 
            int distanceX, int distanceY){
        switch(dir){
            case BACK: return origin.add(new Location(0, -distanceY));
            case FRONT: return origin.add(new Location(0, distanceY));
            case LEFT: return origin.add(new Location(-distanceX, 0));
            case RIGHT: return origin.add(new Location(distanceX, 0));
            case BACKLEFT: return origin.add(new Location(-distanceXdiagonal, -distanceYdiagonal));
            case FRONTRIGHT: return origin.add(new Location(distanceXdiagonal, distanceYdiagonal));
            case BACKRIGHT: return origin.add(new Location(distanceXdiagonal, -distanceYdiagonal));
            case FRONTLEFT: return origin.add(new Location(-distanceXdiagonal, distanceYdiagonal));
            default: return null;
        }
    }
    
    public boolean isEqualTo(Location b){
        return (this.getX().equals(b.getX()) && this.getY().equals(b.getY()));
    }

    public static Integer getHypotenuseBetween(Location a, Location b){
        if(a != null && b != null){
            Location difference =  a.subtract(b);
            return (int) Math.round(
                    Math.sqrt(
                            Math.pow(difference.getX(), 2) + 
                            Math.pow(difference.getY(), 2)
                    ));
        }
        return null;
    }
    /**
     * Used to scale down the difference between two locations.
     * The left corner's location remains the same after scaling.
     * The scaling is done on the X and Y values that are biggest.
     * @param decimal > 0
     * @param a Location a
     * @param b Location b
     */
    public static void scaleDifference(float decimal, Location a , Location b){
        if(decimal == 1f){ return; }
        int biggestX = Location.getBiggestX(a, b).getX();
        int biggestY = Location.getBiggestY(a, b).getY();
        int smallestX = Location.getSmallestX(a, b).getX();
        int smallestY = Location.getSmallestY(a, b).getY();

        int xBig = (int)Math.round(
                ((biggestX-smallestX)*decimal + smallestX));
        int yBig = (int)Math.round(
                ((biggestY-smallestY)*decimal + smallestY));
        
        if(a.isEqualTo(Location.getBiggestX(a, b))){
            a.setX(xBig);
        } else {
            b.setX(xBig);
        }
        
        if(b.isEqualTo(Location.getBiggestY(a, b))){
            b.setY(yBig);
        } else {
            a.setY(yBig);
        }
    }
    /**
     * Multiplies the current X and Y by the decimal.
     * @param decimal 
     */
    public void multiply(float decimal){
        this.setX((int) Math.round(this.getX()*decimal));
        this.setY((int) Math.round(this.getY()*decimal));
    }
    /**
     * Multiplies the smallest X and Y values of the two locations
     * and keeps the right lower corner's distance to the left corner.
     * @param decimal
     * @param a
     * @param b 
     */
    public static void multiplyRelativity(float decimal, Location a , Location b){
        if(decimal == 1f){ return; }
        int biggestX = Location.getBiggestX(a, b).getX();
        int biggestY = Location.getBiggestY(a, b).getY();
        int smallestX = Location.getSmallestX(a, b).getX();
        int smallestY = Location.getSmallestY(a, b).getY();
                
        int xDiff = biggestX-smallestX;
        int yDiff = biggestY-smallestY;
        
        smallestX = Math.round(smallestX*decimal);
        smallestY = Math.round(smallestY*decimal);
        
        if(a.isEqualTo(Location.getBiggestX(a, b))){
            a.setX(smallestX+xDiff);
            b.setX(smallestX);
        } else {
            b.setX(smallestX+xDiff);
            a.setX(smallestX);
        }
        
        if(b.isEqualTo(Location.getBiggestY(a, b))){
            b.setY(smallestY+yDiff);
            a.setY(smallestY);
        } else {
            a.setY(smallestY+yDiff);
            b.setY(smallestY);
        }
    }
    
    public Location negate(){
        return new Location(-this.getX(), -this.getY());
    }
    
    /**
     * Returns a new Location which is rotated
     * 90 degrees clockwise.
     * (Solved with an equation beforehand)
     * @return
     */
    public Location getRotatedLocationSub90() {
    	return new Location(this.getY(), -this.getX());
    }
    
    public static List<Location> getRotatedLocationsSub90(List<Location> locs){
    	ArrayList<Location> locs2 = new ArrayList<>();
    	for(Location loc: locs) {
    		if(loc == null) continue;
    		locs2.add(loc.getRotatedLocationSub90());
    	}
    	return locs2;
    }
    
    public static List<Location> getDistinctLocations(List<Location> locs){
    	ArrayList<Location> locs2 = new ArrayList<>();
    	for(Location loc: locs) {
    		if(loc == null) continue;
    		boolean exists = false;
    		for(Location locCheck: locs2) {
    			if(locCheck.getY() == loc.getY() 
    					&& locCheck.getX() == loc.getX()) {
    				exists = true;
    				break;
    			}
    		}
    		if(!exists) locs2.add(loc);
    	}
    	return locs2;
    }
    
    /**
     * Return a the absolute XY distance between
     * this and b. That is the difference between
     * the two location in x and y axis added together.
     * @param b
     * @return int representing distance.
     */
    public int getXYDistance(Location b) {
    	if(b == null) return 0;
    	int y = Math.abs((b.getY() - this.getY()));
    	int x = Math.abs((b.getX() - this.getX()));
    	return x + y;
    }
    
    /**
     * Returns the closest Location.
     * @param locs
     * @return
     */
    public Location getClosest(List<Location> locs) {
    	Integer leastMoves = null;
    	Location closest = null;
    	for(Location loc: locs) {
    		if(loc == null) continue;
    		if(leastMoves == null 
    				|| loc.getXYDistance(this) < leastMoves) {
    			leastMoves = loc.getXYDistance(this);
    			closest = loc;
    		}
    	}
    	return closest;
    }
    
    
    /**
  * Used to get distance relative to a location from another location.
  * @param relativeTo The location the other location is relative to.
  * @param relative The location that is the relative.
  * @return 
  */
 public static Location getRelativeDistance(Location relativeTo, 
         Location relative){
     return relative.subtract(relativeTo);
 }
    
    
}
