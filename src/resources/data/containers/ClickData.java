/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import entities.foundation.Entity;

/**
 *
 * @author Sammyy
 */
public class ClickData {
    
    private Location clickLoc = null;
    private Entity lastClickedEntity = null;
    private boolean leftMouseClick = true;
    
    public ClickData(Location loc, Entity clicked, boolean leftMouseClick){
        this.clickLoc = loc;
        this.lastClickedEntity = clicked;
        this.leftMouseClick = leftMouseClick;
    }
    
    /**
     * Whether mouse click was done with left button or otherwise.
     * @return
     */
    public boolean isLeftClick() {
    	return this.leftMouseClick;
    }
    
    public Location getLocation(){
        return this.clickLoc;
    }
    
    public Entity getLastClickedEntity(){
        return this.lastClickedEntity;
    }
    
}
