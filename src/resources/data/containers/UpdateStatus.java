package resources.data.containers;

/**
 * Used to give info about when an object should be added/replaced, deleted or has
 * been updated.
 * @author Sammyy
 *
 */
public enum UpdateStatus {
	UPDATED(1), DELETE(0), UPDATE(2), SMALL_UPDATE(3);
	private int updateStatus;
	private UpdateStatus(int value) {
		this.updateStatus = value;
	}
	
	public int getValue() {
		return updateStatus;
	}
	
	public static UpdateStatus getFrom(int value) {
		switch(value) {
		case 0: return DELETE;
		case 1: return UPDATED;
		case 2: return UPDATE;
		case 3: return SMALL_UPDATE;
		}
		return null;
	}
}
