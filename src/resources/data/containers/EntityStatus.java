package resources.data.containers;

import entities.foundation.Entity;

/**
 * A datacontainer for an Entity and its #UpdateStatus
 * Used for requesting updates to Client.
 * @author Sammyy
 *
 */
public class EntityStatus<T extends Entity> {
	private T e = null;
	private UpdateStatus status = null;
	public T getEntity() {
		return e;
	}
	public UpdateStatus getStatus() {
		return status;
	}
	public EntityStatus(T e, UpdateStatus status) {
		this.e = e;
		this.status = status;
	}
}
