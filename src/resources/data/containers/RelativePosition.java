/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.data.containers;

import entities.foundation.Entity;

/**
 * Position relative to another entity.
 * @author Sammyy
 */
public enum RelativePosition {
    BELOW, ABOVE, RIGHT, LEFT, BELOW_LEFT, BELOW_RIGHT, ABOVE_LEFT, ABOVE_RIGHT;
    
}
