/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.dataHandlers;

import utilities.abstracts.Resource;

/**
 * Used to handle and store temporary keypresses from
 * client.
 * @author Sammyy
 */
public class KeyInputHandler extends Resource<String>{
    /**
     * Input that is not ready for delivery.
     */
    private String gatheredInput;
    /**
     * A message that has been 
     */
    private String completeMessage;
    /**
     * Used to indicate backspace was pressed
     * when there is no previous keypress data
     */
    private boolean backspace = false;
    private boolean enter = false;
    /**
     * Whether to listen for enter input to
     * transfer gathered input to complete message.
     */
    private boolean actionOnEnter = true;
     /**
     * Whether to listen for backspace input to
     * shorten message.
     */
    private boolean actionOnBackspace = true;
    
    public KeyInputHandler(){
        this.clearAllInput();
    }
    
    public void mergeKeyInputHandler(KeyInputHandler k){
        backspace = k.isBackspacePressed();//Reset
        enter = k.isEnterPressed();
        k.transferGatheredInput();
        this.addPressedKeyChar(k.getCompleteMessage());
    }
    
    public void setActionOnEnter(boolean bool){
        this.actionOnEnter = bool;
    }
    
     public void setActionOnBackspace(boolean bool){
        this.actionOnBackspace = bool;
    }
    
    /**
     * Enter is stored as a value if no data exists, otherwise
     * it will transfer input to complete message. Backspace will
     * be stored as a value if no data exists, and boolean for backspace
     * stored temporarily, otherwise remove last letter.
     * @param keyValue 
     */
    public void addPressedKeyChar(String keyValue){
        if(keyValue.equalsIgnoreCase("enter") || this.isEnterPressed()){
            if(this.getLengthGatheredInput() > 0 && this.actionOnEnter) transferGatheredInput();
            this.enter = true;
        } else if((keyValue.equalsIgnoreCase("backspace") || this.isBackspacePressed()) && this.actionOnBackspace){
            String newString = KeyInputHandler.getStringExceptLastChar(
                    gatheredInput);
            backspace = true;
            if(newString != null){
                gatheredInput = newString;
            }
        } else {gatheredInput += keyValue;}
    }
    /**
     * Use when done gathering input to
     * transfer to complete message. Is automatically
     * used when pressed enter key.
     */
    public void transferGatheredInput(){
        if(!gatheredInput.isEmpty()){
        completeMessage = gatheredInput;
        gatheredInput = "";
        }
    }
    
    public int getLengthGatheredInput(){
        return gatheredInput.length();
    }
    
    public int getLengthCompleteMessage(){
        return completeMessage.length();
    }
    
    public String getCompleteMessage(){
        String msg = completeMessage;
        this.completeMessage = "";
        return msg;
    }
    
    public boolean hasCompleteMessage(){
        return (!this.completeMessage.isEmpty());
    }
    
    public void clearAllInput(){
        this.gatheredInput = "";
        this.completeMessage = "";
        this.backspace = false;
        this.enter = false;
    }
    
    
    public String getGatheredInput() {
        return gatheredInput;
    }
    /**
     * @return boolean
     */
    public boolean isBackspacePressed(){
        return backspace;
    }
    
    /**
     * @return boolean
     */
    public boolean isEnterPressed(){
        return enter;
    }
    
    public static String getStringExceptLastChar(String str){
        return KeyInputHandler.getStringExceptLastChar(str, null);
    }
    
    public static String getStringExceptLastChar(String str, String returnValueNullCase){
        String returnStr = returnValueNullCase;
        if(str.length() > 0){
            returnStr = str.substring(0, str.length()-1);
            returnStr = (returnStr == null) ? "" : returnStr;
        }
        return returnStr;
    }
    
}
