/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.dataHandlers;

import resources.data.containers.Location;
import utilities.abstracts.Resource;

/**
 * Object used to store information about what's
 * happening with the mouse movement and how to interpret them.
 * @author Sammyy
 */
public class MouseHandler extends Resource<Object>{
    
    public static final String LOCATION_MOVE_MOUSE = "movedMouse";
    public static final String LOCATION_UP_MOUSE = "releasedMouse";
    public static final String LOCATION_DOWN_MOUSE = "pressedMouse";
    public static final String LEFT_MOUSE_CLICK = "leftMouseClick";
    
    private boolean isBeingDragged = false;
    /**
     * The last recorded mouselocation for an event
     * before the current event.
     */
    private Location lastPreviousLocation = null;
    /**
     * Whether last button clicked on mouse was left
     * or right. Null means no previous click.
     */
    private Boolean lastMouseClickWasLeft = null;
    
    /**
     * 
     * @param mouseUp
     * @param mouseDown
     * @param mouseMove
     * @param leftMouseClick
     */
    public MouseHandler(Location mouseUp, Location mouseDown,
            Location mouseMove, Boolean leftMouseClick){
        this.addProperty(LOCATION_UP_MOUSE, mouseUp);
        this.addProperty(LOCATION_DOWN_MOUSE, mouseDown);
        this.addProperty(LOCATION_MOVE_MOUSE, mouseMove);
        this.addProperty(LEFT_MOUSE_CLICK, leftMouseClick);
    }
    
    /**
     * Last recorded location of the mouse before
     * the current mouse event.
     * @return 
     */
    public Location getLastPreviousLocation(){
        return this.lastPreviousLocation;
    }
    
    private void updateLastPreviousLocation(MouseHandler previousHandle){
        if(previousHandle != null){
            if(previousHandle.getLastPreviousLocation() != null &&
                    previousHandle.getCurrentLocation() == null){
                this.lastPreviousLocation = previousHandle.getLastPreviousLocation();
            } else if(previousHandle.getCurrentLocation() != null){
                this.lastPreviousLocation = previousHandle.getCurrentLocation();
            }
        }
    }
    
    /**
     * Update values for mousehandler with new values and store
     * previous handlers important values.
     * @param newHandler
     */
    public synchronized void updateMouseHandler(MouseHandler newHandler){
        MouseHandler previousHandler = this.getCopy();
        //update
        updateLastPreviousLocation(previousHandler);
        this.setBeingDragged(newHandler);
        this.lastMouseClickWasLeft = getLeftMouseClick();
        //construct
        this.clearProperties();
        this.addProperty(LEFT_MOUSE_CLICK, newHandler.getLeftMouseClick());
        this.addProperty(LOCATION_UP_MOUSE, newHandler.getUpMouseLocation());
        this.addProperty(LOCATION_DOWN_MOUSE, newHandler.getDownMouseLocation());
        this.addProperty(LOCATION_MOVE_MOUSE, newHandler.getMoveMouseLocation());
    }
    
    public boolean isBeingDragged(){
    	return this.isBeingDragged;
    }

    
    /**
     * The event where mouse is released.
     * @return 
     */
    public Location getUpMouseLocation(){
        return (Location) this.getProperty(LOCATION_UP_MOUSE);
    }
    
    /**
     * The event where mouse is pressed.
     * @return 
     */
    public Location getDownMouseLocation(){
        return (Location) this.getProperty(LOCATION_DOWN_MOUSE);
    }   
    
    /**
     * The event where mouse is moved.
     * @return 
     */
    public Location getMoveMouseLocation(){
        return (Location) this.getProperty(LOCATION_MOVE_MOUSE);
    }  
    
    /**
     * 
     * @return True if left mouse click otherwise false
     */
    public Boolean getLeftMouseClick() {
    	if(this.isClicking()) {
        	Boolean isLeft = (Boolean) this.getProperty(LEFT_MOUSE_CLICK);
        	return isLeft;
    	} else if(this.hasReleasedClick()){
    		return this.lastMouseClickWasLeft;
    	} 
    	return this.lastMouseClickWasLeft;
    }
    
    public Location getCurrentLocation(){
        if(this.getMoveMouseLocation() != null){
            return this.getMoveMouseLocation();
        } else if(this.getUpMouseLocation() != null){
            return this.getUpMouseLocation();
        } else if(this.getDownMouseLocation() != null){
            return this.getDownMouseLocation();
        }
        return null;
    }
    /**
     * True returned while use tries to drag something to a
     * location.
     * @return boolean
     */
    public void setBeingDragged(MouseHandler newHandler){
    	if(newHandler.hasReleasedClick()) {
    		this.isBeingDragged = false;
    	} else if(newHandler.isClicking() || 
    			(newHandler.getMoveMouseLocation() != null && isBeingDragged)) {
    		this.isBeingDragged = true;
    	} else {
    		this.isBeingDragged = false;
    	}
    }
    
    public boolean isClicking(){
        if(this.getDownMouseLocation() != null){
            return true;
        }
        return false;
    }
    
    public boolean hasReleasedClick(){
        if(this.getUpMouseLocation() != null){
            return true;
        }
        return false;
    }
    
    public MouseHandler getCopy(){
        return new MouseHandler(this.getUpMouseLocation(),
        this.getDownMouseLocation(), this.getMoveMouseLocation(), this.getLeftMouseClick());
    }
    
}
