package start;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import entities.foundation.Entity;
import entities.views.InputEntity;
import entities.views.ViewerEntity;
import resources.data.containers.ClickData;
import resources.data.containers.EntityStatus;
import resources.data.containers.Location;
import resources.data.containers.UpdateStatus;
import resources.dataHandlers.KeyInputHandler;
import resources.dataHandlers.MouseHandler;
import utilities.handlers.FocusHandler;
import utilities.handlers.MouseActionHandler;
import utilities.interfaces.ClickListener;
import utilities.interfaces.IOperator;
import utilities.interfaces.KeyInputListener;
import utilities.interfaces.MouseMoveListener;

/**
 * Represents a Viewer.
 * A Viewer can add ViewerEntity's to its view, and
 * add controller input to be able to use those
 * ViewerEntities.
 * 
 * A 
 * @author Sammyy
 *
 */
public abstract class Viewer implements IOperator {

    /**
     * Handles mouseinput to give usable data about
     * what mouse is doing
     */
    private MouseHandler mouseHandler;
    
    private KeyInputHandler keyInputHandler;
    /**
     * Handles input from MouseHandler and does actions with it.
     */
    private MouseActionHandler mouseActionHandler;
    /**
     * Handles focused ViewerEntitys.
     */
    private FocusHandler focusHandler;
    
    /**
     * Entities only the individual player should see,
     * such as: TextBoxes, inputforms and messages.
     */
    private LinkedList<ViewerEntity> views;
    private List<EntityStatus<ViewerEntity>> viewUpdates;
	
	public Viewer() {
		views = new LinkedList<>();
		viewUpdates = new ArrayList<>();
		this.mouseHandler = new MouseHandler(null, null, null, null);
		this.keyInputHandler = new KeyInputHandler();
		this.focusHandler = new FocusHandler();

		this.mouseActionHandler = new MouseActionHandler(this, mouseHandler, focusHandler,
				views);
	}
	
	/**
	 * Get focused ViewerEntity.
	 * @return ViewerEntity or null
	 */
	public ViewerEntity getFocused() {
		return (ViewerEntity) focusHandler.getFocusedEntity();
	}
	
	/**
	 * Check if last click was left or right click.
	 * @return
	 */
	public boolean getLeftClick() {
		return this.mouseHandler.getLeftMouseClick();
	}

	@Override
	public void addOverlayUpdate(ViewerEntity e, UpdateStatus status) {
		boolean viewIsNew = (!this.hasOverlayEntity(e));
		boolean added;
		synchronized (views) {
	    	if(viewIsNew) {
	    		views.add(e);
	    	} else {
	    		views.remove(e);//Delete old sorting for view
	    		views.add(e);//Display on-top of others
	    	}
		}
		synchronized (viewUpdates) {
	    	EntityStatus<ViewerEntity> es = new EntityStatus<>(e, status);
	    	added = this.viewUpdates.add(es);
		}
    	if(added) this.sendUpdates();
	}
	
	public void addOverlayUpdate(ViewerEntity e) {
		this.addOverlayUpdate(e, UpdateStatus.UPDATE);
	}
	
	public List<ViewerEntity> getViews(){
		return new ArrayList<>(this.views);
	}
	
	/**
	 * Send all viewUpdates
	 * A decoder needs to be created on Client.
	 */
	private void sendUpdates() {
		synchronized (viewUpdates) {
			for(EntityStatus<ViewerEntity> eStatus: viewUpdates) {
				ViewerEntity view = eStatus.getEntity();
				UpdateStatus status = eStatus.getStatus();
				this.handleEncodedUpdate(view.getEncodedData(status));
			}
			viewUpdates.clear();
		}
	}
	
	/**
	 * Override this method to get a String of one single
	 * encoded ViewerEntity update that should be shown in
	 * the window.
	 * 
	 * To decode this data use {@link ViewerDecoder}.
	 * @param update
	 */
	protected abstract void handleEncodedUpdate(String update);
	
	@Override
	public boolean hasOverlayEntity(ViewerEntity e) {
		synchronized(views) {
	    	return views.contains(e);
		}
	}

	@Override
	public boolean removeOverlayEntity(ViewerEntity e) {
		boolean removed;
		synchronized(views) {
			removed = this.views.remove(e);
		}
		synchronized(viewUpdates) {
	    	EntityStatus<ViewerEntity> es = new EntityStatus<>(e, UpdateStatus.DELETE);
	    	if(removed) removed = this.viewUpdates.add(es);
		}
		this.sendUpdates();
		
		if(focusHandler.getFocusedEntity() != null) {
			focusHandler.setFocus(null);//Set focus to null
			this.keyInputHandler.clearAllInput();
		}
		
		return removed;
	}
	
	/**
     * Handle new Mouse controller data
     * @param mHandler
     */
    public synchronized void setMouseHandler(MouseHandler mHandler) {
        this.mouseHandler.updateMouseHandler(mHandler);
        this.mouseActionHandler.updateActions();
    }

    /**
     * Handle new Keyboard input data
     * @param kHandler
     */
    public synchronized void setKeyInputHandler(KeyInputHandler kHandler){
        Entity focused = this.focusHandler.getFocusedEntity();
        
        if(focused != null) {
            this.keyInputHandler.mergeKeyInputHandler(kHandler);//Merge new input
            
        	if(focused instanceof InputEntity) {
                ((InputEntity) focused).fetchInput(this.keyInputHandler);//Update its input
        	} else if(focused instanceof KeyInputListener){
        		((KeyInputListener) focused).onKeyInput(
        				this.keyInputHandler.getGatheredInput());//Update its input
        	}
        }
        
    }

}
