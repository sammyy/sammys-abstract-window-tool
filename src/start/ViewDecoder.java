package start;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import entities.foundation.Entity;
import resources.data.containers.Dimensions;
import resources.data.containers.KeyValue;
import resources.data.containers.Location;
import resources.data.containers.UpdateStatus;

/**
 * Decodes encoded status updates for ViewerEntities.
 * @author Sammyy
 *
 */
public class ViewDecoder {
	
	public static void main(String[] args) {
		test();
	}
	
	public static void test() {
		ViewerData viewData = decode("20001000110000800100Is3:101s5:uniids3:9990011s1:0s1:0");
		System.out.println(viewData.getzValue().equals("999"));
		System.out.println(viewData.getDataValue().equals("101"));
		System.out.println(viewData.getUniqueId().equals("uniid"));
		viewData = decode("2000100011000s5:Times12s6:yellow100Is3:101s5:uniids3:9990011s1:0s1:0");
		System.out.println(viewData.getDim().getFontName().equals("Times"));
		System.out.println(viewData.getDim().getFontSize().equals("12"));
		System.out.println(viewData.getDataValue().equals("101"));
		System.out.println(viewData.getUniqueId().equals("uniid"));
		viewData = decode("300010001s5:uniid0011000001");
		System.out.println(viewData.getAnimation() == 1);
		System.out.println(viewData.getLoc());
		System.out.println(viewData.getzValue().equals("1"));
		System.out.println(viewData.getDataValue() == null);
		System.out.println(viewData.getUniqueId().equals("uniid"));
		viewData = decode("20000000003500300100Is4:9000s9:100000000s3:3000000s1:110000000002500070100Is10:InputFields9:100000001s1:10000s1:0:37");
		System.out.println(viewData.getChildren().get(0).getDataValue());
		viewData = decode("20000000003500300100Is4:9000s9:100000002s3:3000000s1:110000000002500070100"
				+ "Is10:InputFields9:100000003s1:10000s1:11000500270250s10:BlipNormal15s7:#000000100Ts5:jhgf s9:100000007s1:10000s1:0:28210");
		System.out.println(viewData.getChildren().get(0).getChildren().get(0).getDim().getFontName());
		viewData = decode("20000000003500300100Is4:9000s9:100000038s3:3000000s1:110000000002500090100Is4:9001s9:1000000"
				+ "39s1:10000s1:11000500370250s5:Arial15s7:#000000100Ts3:dh s9:100000040s1:10000s1:0");
		System.out.println(viewData.getChildren().get(0).getChildren().get(0).getDim().getFontName());
		viewData = decode("20000000003500300100Is4:9000s9:100000002s3:3000000s1:210000000002500090100Is4:9001s9:100000003s1:10000s1:0"+
				"10000000002500090100Is4:5555s9:100000003s1:10000s1:1"+
				"10000000002500090100Is4:9001s9:100000003s1:10000s1:0");
		System.out.println(viewData.getChildren().get(1).getChildren().get(0).getDataValue());
		viewData = decode("20000000005110272100Is4:9002s10:1581289138s3:2720000s1:710025001500500050100Is4:2000s9:995320011s1:10000"
				+ "s1:11000000000006s5:Arial12s7:#000000100Ts1:1s9:300859678s1:10000s1:010120001500500050100Is4:2003s9:660411951s1:"
				+ "20000s1:11000000000006s5:Arial12s7:#000000100Ts1:1s9:915476183s1:10000s1:010215001500250065100Is4:5000s10:1258249"
				+ "227s1:30000s1:11000000000006s5:Arial12s7:#000000100Ts1:5s10:1561917185s1:10000s1:010310001500650039100Is4:5001"
				+ "s10:1517093760s1:40000s1:11000000000006s5:Arial12s7:#000000100Ts1:2s9:116364746s1:10000s1:0104050015003800651"
				+ "00Is4:5003s9:420337243s1:50000s1:11000000000006s5:Arial12s7:#000000100Ts1:1s9:830315529s1:10000s1:010025011000"
				+ "380065100Is4:5004s10:1624580458s1:60000s1:11000000000006s5:Arial12s7:#000000100Ts1:1s10:1840388947s1:10000s1:01"
				+ "0120011000380065100Is4:5005s10:1552257670s1:70000s1:11000000000006s5:Arial12s7:#000000100Ts1:1s8:72019458s1"
				+ ":10000s1:0:1420");
		System.out.println(viewData.getChildren().get(2).getChildren().get(0).getDataValue());
		System.out.println(viewData.getChildren().get(0).getChildren().get(0).getDataValue());
	}
	
	public static ViewerData decode(String encodedData) {
		int statusValue = Integer.parseInt(encodedData.charAt(0) + "");
		UpdateStatus status = UpdateStatus.getFrom(statusValue);
		switch(status.getValue()) {
		case 0: return decodeUpdate(encodedData);//DELETE
		case 1: return decodeUpdate(encodedData); //UPDATE
		case 2: return decodeUpdate(encodedData); //UPDATE
		case 3: return decodeSmallUpdate(encodedData);//SMALL
		}
		return null;
	}
	
	/**
	 * ViewerData contains data that has been decoded
	 * from an encoded ViewerEntity.
	 * @author Sammyy
	 *
	 */
	public static class ViewerData{
		/**
		 * Type of update
		 */
		private UpdateStatus status = null;
		private Location loc = null;
		private Dimensions dim = null;;
		/**
		 * @see Entity#TYPE_IMAGE etc..
		 */
		private String type = null;
		private Integer brightness = null;
		/**
		 * Represents the src/path for an image
		 * or an identifier for an image/texture
		 */
		private String dataValue = null;
		private String zValue = null;
		/**
		 * Unique id to identify existing Views to
		 * update/change.
		 */
		private String uniqueId = null;
		/**
		 * Animation to do(if any)
		 */
		private Integer animation = null;
		/**
		 * Any extra input of encoded data.
		 */
		private String extraEncodedData = null;
		/**
		 * Buttons, Text, Fields etc. Positioned
		 * relative to upper-left corner of
		 * this ViewerData.
		 */
		private List<ViewerData> children = null;
		/**
		 * Used to store ViewerData for {@link UpdateStatus#UPDATE} or
		 * DELETE
		 * @param loc
		 * @param dim
		 * @param brightness
		 * @param type Type of View to display. @see Entity
		 * @param dataValue Represents the src/path for an image
		 * or an identifier for an image/texture
		 * @param uniqueId Unique id to identify existing Views to
		 * update/change.
		 * @param zValue
		 * @param animation
		 * @param extraEncodedData
		 * @param children
		 */
		public ViewerData(UpdateStatus status, Location loc,
				Dimensions dim, int brightness,
				String type, String dataValue,
				String uniqueId, String zValue, 
				int animation, String extraEncodedData,
				List<ViewerData> children) {
			this.status = status;
			this.loc = loc;
			this.dim = dim;
			this.brightness = brightness;
			this.type = type;
			this.dataValue = dataValue;
			this.uniqueId = uniqueId;
			this.zValue = zValue;
			this.animation = animation;
			this.extraEncodedData = extraEncodedData;
			this.children = children;
		}
		/**
		 * Used to store ViewerData for {@link UpdateStatus#SMALL_UPDATE}
		 * @param loc
		 * @param brightness
		 * @param uniqueId Unique id to identify existing Views to
		 * update/change.
		 * @param zValue
		 * @param animation
		 */
		public ViewerData(Location loc, String uniqueId,
				int animation, int brightness, int zValue) {
			this.status = UpdateStatus.SMALL_UPDATE;
			this.loc = loc;
			this.brightness = brightness;
			this.uniqueId = uniqueId;
			this.zValue = zValue + "";
			this.animation = animation;
		}
		
		public UpdateStatus getStatus() {
			return this.status;
		}
		
		public Location getLoc() {
			return loc;
		}
		public Dimensions getDim() {
			return dim;
		}
		public String getType() {
			return type;
		}
		public int getBrightness() {
			return brightness;
		}
		/**
		 * dataValue Represents the src/path for an image
		 * or an identifier for an image/texture
		 * @return
		 */
		public String getDataValue() {
			return dataValue;
		}
		public String getzValue() {
			return zValue;
		}
		/**
		 * uniqueId Unique id to identify existing Views to
		 * update/change.
		 * @return
		 */
		public String getUniqueId() {
			return uniqueId;
		}
		/**
		 * Animation to do(if any)
		 */
		public int getAnimation() {
			return animation;
		}
		public String getExtraEncodedData() {
			return extraEncodedData;
		}
		/**
		 * ViewerData such as Button, InputField that
		 * are position relative to upper-left corner
		 * of this ViewerData.
		 * @return
		 */
		public List<ViewerData> getChildren() {
			return children;
		}
	}
	
	/**
	 * Decode a UpdateStatus.SMALL_UPDATE. Only contains
	 * Location, uniqueId:String, animation:int, brightness:int,
	 * zValue:String. Within ViewerData.
	 * @param encodedData
	 * @return
	 */
	public static ViewerData decodeSmallUpdate(String encodedData) {
		
		String xVal = encodedData.substring(1, 5);
		String yVal = encodedData.substring(5, 9);
		int x = excludeZeros(xVal);
		int y = excludeZeros(yVal);
		Location loc = new Location(x, y);
		encodedData = encodedData.substring(9);
		
		KeyValue<Integer, String> kValue = decodeString(encodedData);
		int index = kValue.getKey();
		encodedData = encodedData.substring(index);
		String uniqueId = kValue.getValue();
		
		int animation = excludeZeros(encodedData.substring(0, 3));
		encodedData = encodedData.substring(3);
		
		int brightness = excludeZeros(encodedData.substring(0, 3));
		encodedData = encodedData.substring(3);
		
		int zValue = excludeZeros(encodedData.substring(0, 4));
		encodedData = encodedData.substring(4);
		ViewerData parent = new ViewerData(loc, uniqueId, 
				animation, brightness, zValue);
		
		return parent;
		
	}
	
	private static ViewerData decodeUpdate(String encodedData) {
		return decodeUpdateHelper(encodedData, 0).getValue();
	}
	
	private static KeyValue<Integer, ViewerData> decodeUpdateHelper(String encodedData, int totalIndex) {
		int statusValue = Integer.parseInt(encodedData.charAt(0) + "");
		UpdateStatus status = UpdateStatus.getFrom(statusValue);
		
		String xVal = encodedData.substring(1, 5);
		String yVal = encodedData.substring(5, 9);
		int x = excludeZeros(xVal);
		int y = excludeZeros(yVal);
		Location loc = new Location(x, y);
		encodedData = encodedData.substring(9); totalIndex += 9;
		
		KeyValue<Integer, Dimensions> decoded = 
				decodeDimensions(encodedData);
		int index = decoded.getKey(); totalIndex += index;
		encodedData = encodedData.substring(index);
		Dimensions dim = decoded.getValue();
		
		int brightness = excludeZeros(encodedData.substring(0, 3));
		encodedData = encodedData.substring(3); totalIndex += 3;
		
		String type = "";
		switch(encodedData.charAt(0)) {
		case 'T': type = Entity.TYPE_TEXT; break;
		case 'I': type = Entity.TYPE_IMAGE; break;
		case 'E': type = Entity.TYPE_ELLIPS; break;
		case 'R': type = Entity.TYPE_RECTANGLE; break;
		case 'L': type = Entity.TYPE_LINE; break;
		}
		encodedData = encodedData.substring(1); totalIndex += 1;

		KeyValue<Integer, String> kValue = decodeString(encodedData);
		index = kValue.getKey(); totalIndex += index;
		encodedData = encodedData.substring(index);
		String dataValue = kValue.getValue();
		
		kValue = decodeString(encodedData);
		index = kValue.getKey(); totalIndex += index;
		encodedData = encodedData.substring(index);
		String uniqueId = kValue.getValue();
		
		kValue = decodeString(encodedData);
		index = kValue.getKey(); totalIndex += index;
		encodedData = encodedData.substring(index);
		String zValue = kValue.getValue();
		
		int animation = excludeZeros(encodedData.substring(0, 3));
		encodedData = encodedData.substring(3); totalIndex += 3;
		
		String hasExtra = encodedData.substring(0, 1);
		encodedData = encodedData.substring(1); totalIndex += 1;
		String encodedExtraData = null;
		
		if(!hasExtra.equals("0")) {
			kValue = decodeString(encodedData);
			index = kValue.getKey(); totalIndex += index;
			encodedData = encodedData.substring(index);
			encodedExtraData = kValue.getValue();
		}
		
		kValue = decodeString(encodedData);
		index = kValue.getKey(); totalIndex += index;
		encodedData = encodedData.substring(index);
		String childEntitiesCount = kValue.getValue();
		int children = Integer.parseInt(childEntitiesCount);
		
		List<ViewerData> childViews = new ArrayList<>();
		ViewerData parent = new ViewerData(status, loc, dim, brightness,
				type, dataValue, uniqueId, zValue, animation,
				encodedExtraData, childViews);
		
		for(int child = 0; child < children; child++) {
			KeyValue<Integer, ViewerData> kVal = decodeUpdateHelper(encodedData, 0);
			index = kVal.getKey(); totalIndex += index;
			encodedData = encodedData.substring(index);
			ViewerData childView = kVal.getValue();
			childViews.add(childView);
		}
		
		return new KeyValue<>(totalIndex, parent);
		
	}
	
	private static KeyValue<Integer, Dimensions> decodeDimensions(String encoded) {
		String part = encoded.substring(0, 4);
		Dimensions dim;
		int index = 0;
		int width = excludeZeros(part);
		if(encoded.charAt(4) == 's') {//Fontname
			String substr = encoded.substring(4);
			KeyValue<Integer, String> decoded = decodeString(substr);
			int lengthOfDecoded = decoded.getKey();
			String fontName = decoded.getValue();
			//Substr for fontsize
			index = 4 + lengthOfDecoded;
			substr = encoded.substring(index, index + 2);
			int size = excludeZeros(substr);
			index += 2;
			//Decode fontcolour
			substr = encoded.substring(index);
			decoded = decodeString(substr);
			lengthOfDecoded = decoded.getKey();
			String colour = decoded.getValue();
			index += lengthOfDecoded;
			dim = new Dimensions(width + "", fontName, size + "", colour);
		} else {//Height
			part = encoded.substring(4, 8);
			int height = excludeZeros(part);
			dim = new Dimensions(width, height);
			index = 8;
		}
		return new KeyValue<>(index, dim);
	}
	
	/**
	 * 
	 * @param encoded
	 * @return Integer for length of decoded data, and
	 * String for decoded string.
	 */
	private static KeyValue<Integer, String> decodeString(String encoded){
		if(encoded.charAt(0) == 's') {
			String part = encoded.split(":")[0];
			String number = part.substring(1);
			Integer length = Integer.parseInt(number);
			int metaLength = number.length() + 2;
			String decoded = encoded.substring(metaLength, metaLength + length);
			return new KeyValue<Integer, String>(metaLength + length, decoded);
		}
		return null;
	}
	
	private static Integer excludeZeros(String number) {
		String exc = "";
		boolean keepGoing = true;
		for(int i = 0; i < number.length(); i++) {
			if(number.charAt(i) != '0' || i +1 == number.length()) {
				exc = number.substring(i);
				return Integer.parseInt(exc);
			}
		}
		return null;
	}

}
