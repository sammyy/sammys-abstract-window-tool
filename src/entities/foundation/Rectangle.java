/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.foundation;

import utilities.abstracts.Shape2D;

import java.util.ArrayList;
import java.util.List;

import entities.foundation.Entity;
import resources.data.containers.Dimensions;
import resources.data.containers.LineMath;
import resources.data.containers.Location;
import resources.exceptions.MultipleIntersectionsPointsForLineException;
import utilities.interfaces.Childable;

/**
 * Shape Rectangle which can be used to see if something is inside it and
 * displayed.
 * @author Sammyy
 */
public class Rectangle extends Shape2D implements Childable{
	
    private Location beginPath = null;
    private Location lineToA = null;
    private Location lineToB = null;
    private Location lineToC = null;
    
    public Rectangle(Location a, Location b){
        super("");
        super.setDimensions(new Dimensions(0+"",0+""));
        initAll(a, b);
    }

    public Rectangle(String id, Location a, Location b){
        super("");
        super.setDimensions(new Dimensions(0+"",0+""));
        initAll(a, b);
    }
    
    @Override
    public void initAll(Location a, Location b){ 
        super.setA(a);
        super.setB(b);
        
        this.beginPath = Location.getLeftUpperCorner(a, b);
        this.lineToA = new Location(Location.getBiggestX(a, b).getX(),
        Location.getSmallestY(a, b).getY());
        this.lineToB = new Location(Location.getBiggestX(a, b).getX(),
        Location.getBiggestY(a, b).getY());
        this.lineToC = new Location(Location.getSmallestX(a, b).getX(),
        Location.getBiggestY(a, b).getY());
        
        super.setLocation(new Location(0,0));//Needed
    }
    
    @Override
    public boolean isInside(int x, int y) {
        if(x >= Location.getSmallestX(super.getA(), super.getB()).add(this.getLocation()).getX() &&
                x <= Location.getBiggestX(super.getA(), super.getB()).add(this.getLocation()).getX() &&
                y >= Location.getSmallestY(super.getA(), super.getB()).add(this.getLocation()).getY() &&
                y <= Location.getBiggestY(super.getA(), super.getB()).add(this.getLocation()).getY()){
            return true;
        }
        return  false;
    }
    
    public boolean hasCollided(Entity collidedWith) {
        Location feet = collidedWith.getBottomLocation();
        Location head = collidedWith.getHeadLocation();
        int entityWidth = Integer.valueOf(collidedWith.getRealDimensions().getWidth());
        boolean collided = false;

        int yMin = feet.getY() - collidedWith.getDepth();
        int xMax = head.getX()+entityWidth;
        for (int y = feet.getY(); yMin < y; y--) {
            for (int x = head.getX(); x < xMax; x++) {
                collided = this.isInside(x, y);
                if (collided == true) {
                    return collided;
                }
            }
        }
        return collided;
    }

    
    @Override
    public String getEncodedExtraData(){
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        sb.append(super.getEncodedLocation(this.beginPath));
        sb.append(super.getEncodedLocation(this.lineToA));
        sb.append(super.getEncodedLocation(this.lineToB));
        sb.append(super.getEncodedLocation(this.lineToC));
        return sb.toString();
    }

	@Override
	public List<Location> intersectsWith(Shape2D shape) throws MultipleIntersectionsPointsForLineException {
		List<Location> locs = new ArrayList<>();
    	if(shape instanceof Line) {
    		if(((Line) shape).isNonExistant()) return null;
        	locs.addAll(this.intersectsWithLine((Line) shape));
    		return locs;
    	} else if(shape instanceof Rectangle) {
    		
    	} else if(shape instanceof Ellips) {
    		
    	}
    	return locs;
	}
	
	private List<Location> intersectsWithLine(Line line) 
			throws MultipleIntersectionsPointsForLineException{
		List<Line> rectangleAsLines = new ArrayList<>();
		rectangleAsLines = this.getRectangleAsLines();
		
		List<Location> intersections = new ArrayList<>();
		List<Location> intersectionsTemp;
		
		for(Line lineB: rectangleAsLines) {
			intersectionsTemp = lineB.intersectsWith(line);
			if(intersectionsTemp != null 
					&& !intersectionsTemp.isEmpty()) intersections.addAll(intersectionsTemp);
		}
		intersections = Location.getDistinctLocations(intersections);//Improve
		return intersections;
	}
	
	/**
	 * Get the rectangle as 4 lines.
	 * @return
	 */
	private List<Line> getRectangleAsLines(){
		List<Line> lines = new ArrayList<>();
		Line a = new Line(beginPath.getCopy(), lineToA.getCopy(), true);
		Line b = new Line(lineToA.getCopy(), lineToB.getCopy(), true);
		Line c = new Line(lineToB.getCopy(), lineToC.getCopy(), false);
		Line d = new Line(lineToC.getCopy(), beginPath.getCopy(), true);
		//Set parent for relative location
		a.setParentEntity(this);
		b.setParentEntity(this);
		c.setParentEntity(this);
		d.setParentEntity(this);
		lines.add(a); lines.add(b); lines.add(c); lines.add(d);
		return lines;
	}
	
	public static class Test {
		
		/**
		 * (0, 0)
		 * (0, 0) & (4, 4).
		 * Multipleex.
		 * (0,1) & (4, 1)
		 * Tested and WORKS
		 */
		public static void test() {
			Rectangle r = new Rectangle(new Location(0, 4), new Location(4, 0));
			Line l = new Line(new Location(-1, -1), new Location(0, 0), false);
			List<Location> locs;
			try {
				locs = r.intersectsWith(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleExc.");
			}
			
			r = new Rectangle(new Location(0, 4), new Location(4, 0));
			l = new Line(new Location(-1, -1), new Location(4, 4), false);
			try {
				locs = r.intersectsWith(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleExc.");
			}
			
			r = new Rectangle(new Location(0, 4), new Location(4, 0));
			l = new Line(new Location(0, -1), new Location(0, 7), false);
			try {
				locs = r.intersectsWith(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleExc.");
			}
			
			r = new Rectangle(new Location(0, 4), new Location(4, 0));
			l = new Line(new Location(0, 1), new Location(4, 1), false);
			try {
				locs = r.intersectsWith(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleExc.");
			}
			
			r = new Rectangle(new Location(0, 4), new Location(4, 0));
			l = new Line(new Location(0, 2), new Location(2, 0), false);
			try {
				locs = r.intersectsWith(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleExc.");
			}
		}
		
	}

    
}
