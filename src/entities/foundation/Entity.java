/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.foundation;




import entities.views.ViewerEntity;
import entities.views.children.Text;
import resources.data.containers.Dimensions;
import resources.data.containers.EntityClientInfo;
import resources.data.containers.Location;
import resources.data.containers.UpdateStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicLong;

import utilities.abstracts.Shape2D;

/**
 * Entity handling Collision, Moving and image data etc...
 * Serialization for serverresponse.
 * @author Sammyy
 */
public abstract class Entity {
    /**
     * Positioning
     */
    private Location location;
    /**
     * Dimensions for the game
     * objects when being displayed.
     */
    private Dimensions dimensions;
    /**
     * Brightness of it's image/text
     * 0-unlimited
     */
    private int brightness = 100;
    
    /**
     * Representation of an image. Can either be an ID
     * or an img path.
     */
    private String data;
    
    /**
     * -1 Keep previous animation
     * 0 Stop any existing animation
     * 1 walk
     * 2 dance
     */
    private int animation = 0;

	/**
     * Tells which id is free for next Entity to be created
     */
    private static ConcurrentLinkedQueue<Integer> freeUniqueIds = new ConcurrentLinkedQueue<>();
    private static ConcurrentLinkedQueue<Integer> takenUniqueIds = new ConcurrentLinkedQueue<>();
    
    private Integer uniqueId;
    
    public static final String TYPE_TEXT = "TEXT";
    public static final String TYPE_IMAGE = "IMAGE";
    public static final String TYPE_ELLIPS = "ELLIPS";
    public static final String TYPE_RECTANGLE = "RECTANGLE";
    public static final String TYPE_LINE = "LINE";
   
    /**
     * Childentities of the parent entity.
     * Eg: Player can have clothes. Textboxes can have
     * inputfields. Always drawn on top of parent.
     */
    private ConcurrentMap<Integer, Entity> childEntitiesZSorted;
    
    /**
     * Reference to childEntitiesZSorted to avoid concurrency with GSON
     */
    private Collection<Entity> childEntities;
    
    /**
     * Painting index used in ConcurrentMap to be drawn in given
     * order. (in case this entity is a child for instance Clothing)
     */
    private Integer childPaintingIndex = null;

	/**
     * Entity that handles collision for this entity.
     */
    private Entity collisionEntity = null;
    
    /**
     * Last added child to this entity
     */
    private Entity lastAddedChild = null;
    
    /**
     * Whether this Entity is a child of a parent Entity
     * If not then null
     */
    private Entity parent = null;
    
    private static Random random = new Random();
    
    public final Integer getUniqueId() {
    	return this.uniqueId;
    }

    public Entity(String imageId){
        this.data = imageId;
        this.uniqueId = genUniqueId();
        this.initChildEntities();
        initLocation();
        this.setDimensions(new Dimensions(0, 0));//Initial dimensions
    }
    
    private static Integer genUniqueId() {
        Integer uniqueId = freeUniqueIds.poll();
        while(uniqueId == null || takenUniqueIds.contains(uniqueId)) {
        	uniqueId = random.nextInt(Integer.MAX_VALUE); 
        }
        takenUniqueIds.add(uniqueId);
        return uniqueId;
    }
    
    private void initChildEntities() {
    	childEntitiesZSorted = new ConcurrentSkipListMap<>();
        childEntities = childEntitiesZSorted.values();
    }
        
    private void initLocation(){
        Location loc = new Location(0,0);
        setLocation(loc);
    }

    /**
     * Gets Location on canvas.
     * @return
     */
    public Location getLocation() {
    	if(this.getParentEntity() != null) return this.location.add(this.getParentEntity().getLocation());
        return (this.location == null) ? null:
                this.location.getCopy();
    }
    
    /**
     * Gets relative location to parent
     * @return
     */
    public Location getLocation(boolean relativeLocation) {
    	if(!relativeLocation) return this.getLocation();
        return (this.location == null) ? null:
                this.location.getCopy();
    }
    
    /**
     * Sets location for Entity. All childrens Location are relative to 
     * parent.
     * @param location 
     * keeps same relative position)
     */
    protected void setLocation(Location location){
        this.location = location;
    }
    
    /**
     * The painting index for the entity, if the entity is a child of another Entity.
     * @return
     */
    public Integer getChildPaintingIndex() {
		return childPaintingIndex;
	}

    /**
     * The painting index for the entity, if the entity is a child of another Entity.
     * @return
     */
	public void setChildPaintingIndex(Integer paintingIndex) {
		this.childPaintingIndex = paintingIndex;
	}
    


    public Dimensions getDimensions() {
        if(dimensions == null){ return null; }
        return dimensions.getImmutableDimensions();
    }

    protected void setDimensions(Dimensions dim) {
        this.dimensions = dim;
    }

    public int getBrightness() {
        return brightness;
    }

    protected void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public String getType() {
        if(this instanceof Text) {
        	return Entity.TYPE_TEXT;
        } else if(this instanceof Ellips) {
        	return Entity.TYPE_ELLIPS;
        } else if(this instanceof Rectangle) {
        	return Entity.TYPE_RECTANGLE;
        } else if(this instanceof Line) {
        	return Entity.TYPE_LINE;
        } else {
        	return Entity.TYPE_IMAGE;
        }
    }

    public String getData() {
        return data;
    }

    protected void setData(String data) {
        this.data = data;
    }
    /**
     * zIndex used for sorting objects onto screen in
     * correct order. The higher z-index the closer to screen, or
     * on top of other entities.
     * @return 
     */
    public int getzIndex() {
        if(this.getParentEntity() != null) return this.childPaintingIndex;
        Location loc = this.getBottomLocation();
        if(loc != null){
            if(loc.getY() != null){
            	if(loc.getY() < 0) return 0;
                return loc.getY();
            }
        }
        return 1;
    }

    public List<Entity> getChildEntities() {
        return Collections.unmodifiableList(new ArrayList<Entity>(childEntities));
    }

    /**
     * Adds child entity at suggestive index(order), sets parent.
     * Doesn't send changes to Client. Needs to be done manually {@link #requestUpdate(UpdateStatus)}
     * @param childEntity
     * @param index The order which entities are painted and their indices for storing.
     * Not the final index, but a suggestive index
     */
    protected void addChildEntity(Entity childEntity, int index) {
    	int uniqueIndex = this.getUniqueChildIndex(index);
    	this.childEntitiesZSorted.put(uniqueIndex, childEntity);
    	this.lastAddedChild = childEntity;
    	childEntity.setChildPaintingIndex(uniqueIndex);
        childEntity.setParentEntity(this);
        //childEntity.requestUpdate(UpdateStatus.UPDATE);
    }
    
    /**
     * Adds a childEntity and gives a random index(but ascending painting order)
     * Doesn't automatically request an update to client. Needs to be done manually when using
     * this method.
     * @param childEntity
     */
    protected void addChildEntity(Entity childEntity) {
    	this.addChildEntity(childEntity, true);
    }
    
    /**
     * Adds a childEntity and gives a random index(but ascending painting order)
     * Also sets parent and updates major parent and child Entity.
     * @param childEntity
     * @param waitUpdate Waits to send update to client. Useful when adding multiple children at same time.
     * Prevents sending children faculty times.
     */
    protected void addChildEntity(Entity childEntity, boolean waitUpdate) {
    	int uniqueIndex = this.getUniqueChildIndex(1);
    	this.childEntitiesZSorted.put(uniqueIndex, childEntity);
    	this.lastAddedChild = childEntity;
    	childEntity.setChildPaintingIndex(uniqueIndex);
        childEntity.setParentEntity(this);
        if(!waitUpdate) childEntity.requestUpdate(UpdateStatus.UPDATE);
    }
    
    /**
     * Removes child and updates major parent.
     * OBS! No automatic update to Client.
     * @param e Entity for the index
     * @param index Index for the entity
     * @return true if was removed
     */
    protected boolean removeChildEntity(int index){
    	Entity removed;
        removed = this.childEntitiesZSorted.remove(index);
        if(this.lastAddedChild == removed) this.lastAddedChild = null;
        return removed != null;
    }
    
    /**
     * Deletes a child from parent if there is a paintingKey registered.
     * OBS! No automatic update to Client.
     * @param e Entity for the index
     * @return true if was removed
     */
    protected boolean removeChildEntity(Entity e){
    	boolean removed;
    	if(e == null) return false;
    	removed = ((e.getChildPaintingIndex() != null) ? this.childEntitiesZSorted.
        		remove(e.getChildPaintingIndex(), e) : false);
    	if(this.lastAddedChild == e) this.lastAddedChild = null;
        e.setParentEntity(this);
        return removed;
    }
    
    /**
     * Clear all child entities
     * @param andUpdate Whether to send update to client or not
     */
    protected void clearChildEntities(boolean andUpdate){
        Entity child = null;
        if(this.getChildEntities() != null && 
                !this.getChildEntities().isEmpty()){
            child = this.getChildEntities().get(0);
        }
        this.childEntitiesZSorted.clear();
        //Update child to update parent
        if(child != null && andUpdate) child.requestUpdate(UpdateStatus.DELETE);
    }
    
    public Entity getLastAddedChild(){
        return this.lastAddedChild;
    }
    /**
     * The location where the bottom of entity is.
     * For instance the player's feet.
     * @return 
     */
    public Location getBottomLocation(){
        int height = this.getDimensions().getHeight();
        Location location = new Location(0, height);
        return this.getLocation().add(location);
    }
    
    /**
     * Set this entity's collision entity. 
     * @param <T>
     * @param entity 
     */
    protected <T extends Shape2D> void setCollisionEntity(T entity){
        this.collisionEntity = entity;
    }
    
    /**
     * The depth of an entity. Faked 3d dimension in this case z. For instance length of player's feet.
     * @return 
     */
    public int getDepth(){
        return 1;
    }
    
    public void removeCollision(){
        if(this.collisionEntity != null){
            this.removeChildEntity(this.collisionEntity);
            this.collisionEntity = null;
        }
    }
    
    /**
     * Height of the collision area
     * @return 
     */
    public int getCollisionHeight(){
        if (this.collisionEntity != null) {
            return ((Shape2D) this.collisionEntity).getHeight();
        }
        return 0;
    }
    /**
     * Width of the collision area.
     * @return 
     */
     public int getCollisionWidth(){
         if (this.collisionEntity != null) {
             return ((Shape2D) this.collisionEntity).getWidth();
         }
         return 0;
    }
     
    /**
     * Get the upper left corner of the collision object.
     * @return 
     */ 
    public Location getCollisionLeftCorner(){
        if (this.collisionEntity != null) {
            Shape2D s = ((Shape2D) this.collisionEntity);
            return Location.getLeftUpperCorner(s.getA(), s.getB());
        }
        return null;
    } 
    
    public Shape2D getCollisionEntity(){
        return (Shape2D)this.collisionEntity;
    }
    
    /**
     * Scale size of the entity if it is an image.
     * Also scales the collision object's position and size.
     * @param decimal 
     * @param includeCollision whether to include collision when scaling
     */
     public void scale(float decimal, boolean includeCollision){
        if(this.getType().equals(Entity.TYPE_IMAGE) && this.dimensions != null){
            if (this.collisionEntity != null && includeCollision) {
                ((Shape2D) this.collisionEntity).scale(decimal);
            }
            
            int height = this.getDimensions().getHeight();
            int width = this.getDimensions().getWidth();
            width = (int) Math.ceil(width*decimal);
            height = (int) Math.ceil(height*decimal);
            this.dimensions.setImageDimensions(width+"", height+"");
            //TODO real dimensions and head and bottom location
        }
    }
     
     /**
     * {includeDoc}
     * Includes scaling for collision.
     * @param decimal 
     */
     public void scale(float decimal){
        this.scale(decimal, true);
    }
     
    
     /**
      * Returns a unique index for each child in
      * ascending order.
      * @param index Desired index
      * @return A unique index for a child to be added.
      */
     protected int getUniqueChildIndex(int index){
         while(this.childIndexExists(index)) index++;
         return index;
     }
     
     /**
      * Tells whether a child of Entity is already occupying the
      * given index.
      * @param index String with only numbers
      * @return True or false
      */
     private boolean childIndexExists(int index) {
    	 return (this.childEntitiesZSorted.get(index) != null);
     }
     
     /**
      * Turns a number into a string of finalLength. If the number
      * has less digits than finalLength, then zeros are added at the beginning.
      * @param number Converted into a String of finalLength.
      * @param finalLength the length of the String returned.
      * @return Returns a string of length: finalLength, representing a number
      * taking up a desired amount of spaced.
      * @throws IllegalArgumentException Thrown when number contains more digits
      * than finalLength.
      */
     public static final String getZeroFiller(int number, int finalLength){
         StringBuilder strNumberFilled = new StringBuilder();
         String strNumber = number+"";
         if(strNumber.length() > finalLength) throw new IllegalArgumentException();
         for(int i = finalLength - strNumber.length(); i > 0; i--) {
        	 strNumberFilled.append("0");
         }
         strNumberFilled.append(number);
         return strNumberFilled.toString();
     }
     
     /**
      * Location where the left corner closest to the head is.
      * Useful if there is extra transparent space before location of head.
      * Therefore this is used when calculating collision.
      * @return
      */
     public Location getHeadLocation() {
    	 return this.getLocation();
     }
     
     /**
      * An entity can have transparent space which shouldn't be clickable
      * or collidable. Therefore this method is used when calculating collision
      * or interaction, to be able to override the Dimensions with correct ones.
      * @return
      */
     public Dimensions getRealDimensions() {
    	 return this.getDimensions();
     }
     
     /**
      * Encoded data about Entity for client to decode.
      * @return String with encoded data
      */
     public String getEncodedData(UpdateStatus status) {
    	 if(status == UpdateStatus.SMALL_UPDATE) {
    		 return this.getSmallEncodedData(status);
    	 }
    	 StringBuilder data = new StringBuilder();
    	 data.append(status.getValue());
    	 data.append(this.getEncodedLocation(this.getLocation(true)));
    	 data.append(this.getEncodedDimensions());
    	 data.append(this.getEncodedBrightness());
    	 data.append(this.getType().charAt(0));
    	 data.append(this.getEncodedStringMeta(this.getData()));
    	 data.append(this.getEncodedStringMeta(this.getUniqueId()+""));
    	 data.append(this.getEncodedZValue());
    	 data.append(getZeroFiller(animation, 3));
         data.append(this.getEncodedExtraData());
    	 data.append(this.getEncodedStringMeta(this.getChildEntities().size()+""));
    	 for(Entity e: this.getChildEntities()) {
    		 //Child status updated with parent update
    		 data.append(e.getEncodedData(UpdateStatus.UPDATED));
    	 }
    	 return data.toString();
     }
     
     /**
      * Encoded data about Entity for client to decode.
      * OBS! This data only includes small updates such
      * as location updates, direction of player, animationtype
      * @return String with encoded data
      */
     private String getSmallEncodedData(UpdateStatus status) {
    	 StringBuilder data = new StringBuilder();
    	 data.append(status.getValue());
    	 data.append(this.getEncodedLocation(this.getLocation(true)));
    	 data.append(this.getEncodedStringMeta(this.getUniqueId()+""));
    	 data.append(getZeroFiller(animation, 3));
    	 data.append(this.getEncodedBrightness());
    	 data.append(getZeroFiller(this.getIntWithinBounds(this.getzIndex(), 9999, 0), 4));
    	 return data.toString();
     }
     
     /**
      * Reassembles the player on Client and handles
      * the given animation once Entity has requested an update.
      * @param animationIndex
      */
     public void setAnimation(int animationIndex) {
    	 this.animation = animationIndex;
     }
     
     protected int getAnimation() {
    	 return this.animation;
     }
     
     /**
      * Return the output for an encoded value
      * letting the Client get data to decide
      * painting order of Entities in relation to eachother.
      * @return String
      */
     private String getEncodedZValue() {
    	 return this.getEncodedStringMeta(this.getzIndex() + "");
     }
     
     /**
      * Override this to add extra encoded data before last
      * encoded value. OBS! If you override this add "1" at 
      * beginning of String to signal extra encoded data to be
      * read by Client.
      * @return 
      */
     protected String getEncodedExtraData(){
         return "0";
     }
     
     private String getEncodedBrightness() {
    	 return getZeroFiller(this.getIntWithinBounds(getBrightness(), 100, 0), 3);
     }
     
     /**
      * Get encoded Dimensions data for part of server response.
      * @return Data
      */
     private String getEncodedDimensions() {
    	 if(this.getDimensions() == null) throw new NullPointerException();
    	 String data = "";
    	 if(this.getType().equals(Entity.TYPE_TEXT)) {
    		 int width = this.getIntWithinBounds(this.getDimensions().getWidth(), 9999, 0); //No maximum width for zero
    		 String fName = this.getEncodedStringMeta(this.getDimensions().getFontName());
    		 int fontSize = this.getDimensions().getFontSize();
    		 fontSize = this.getIntWithinBounds(fontSize, 99, 1);
    		 String fColour = this.getEncodedStringMeta(this.getDimensions().getFontColor());

    		 data += getZeroFiller(width, 4) + fName + getZeroFiller(fontSize, 2) +
    				 fColour;
    	 } else {
    		 int width = this.getIntWithinBounds(this.getDimensions().getWidth(), 9999, 1);
    		 int height = this.getIntWithinBounds(this.getDimensions().getHeight(), 9999, 1);
    		 data += getZeroFiller(width, 4);
    		 data += getZeroFiller(height, 4);
    	 }
    	 return data;
     }
     
     /**
      * Encoded location data for part of the server resposne.
      * @return String with encoded location
      */
     protected String getEncodedLocation(Location loc) {
    	 if(loc == null) throw new NullPointerException();
    	 int x = this.getIntWithinBounds(loc.getX(), 9999, -999);
    	 int y = this.getIntWithinBounds(loc.getY(), 9999, -999);
    	 String data = getZeroFiller(x, 4) + getZeroFiller(y, 4);
    	 return data;
     }
     
     /**
      * Keeps an int value within given boundaries.
      * A value exceeding boundaries is narrowed down to the closest boundary
      * value.
      * @param value
      * @param maxValue
      * @param minValue
      * @return int value
      */
     protected int getIntWithinBounds(int value, int maxValue, int minValue) {
    	 value = (value > maxValue) ? maxValue : value;
    	 value = (value < minValue) ? minValue : value; 
    	 return value;
     }
     
     /**
      * Encodes string for part of server response with metadata about
      * the string.
      * @param str
      * @return String
      */
     private String getEncodedStringMeta(String str) {
    	 str = "s" + str.length() + ":" + str;
    	 return str;
     }
     
    /**
     * Updates this Entity state on Client(REMOVE/ADD/UPDATE)
     * The update is made as soon as possible
     * Automatically removes, add or changes Entity in the Room
     * or overlay it should be displayed in.
     */
    private boolean requestUpdate(UpdateStatus updateStatus) {
        Entity parent = this.getMajorParent();
        parent = (parent == null) ? this : parent;
        if(parent != null && parent.isVisible()) { //Update parent, thus updates child entity(if visible)
            if(parent instanceof ViewerEntity){
                //Add parent to update list for overlay
                ViewerEntity ve = ((ViewerEntity) parent);
                if(updateStatus == UpdateStatus.DELETE 
                		&&(parent == this || parent == null)) {
                	ve.getOperator().removeOverlayEntity(ve);
                } else {
                	ve.getOperator().addOverlayUpdate(ve, updateStatus);
                }
            }
        }
        
        return true;
    }
    
    protected void setParentEntity(Entity e){
        this.parent = e;
    }    
    
    /**
     * Return null or parent of this Entity.
     */
    public Entity getParentEntity(){
        return this.parent;
    }
    
    public boolean isVisible() {
    	Entity parent = this.getMajorParent();
        parent = (parent == null) ? this : parent;
        if(parent != null && parent.isVisible()) {
        	if(parent instanceof ViewerEntity){
                //Add parent to update list for overlay
                ViewerEntity ve = ((ViewerEntity) parent);
                return ve.isVisible();
            }
        }
        return false;
    }
    
    /**
     * Returns the Major parent Entity highest up
     * the chain. If there is no parent null is
     * returned.
     */
    public Entity getMajorParent(){
        Entity e = this;
        while(e.getParentEntity() != null){
            e = e.getParentEntity();
        }
        return (e == this) ? null : e;
    }
    
    /**
     * Checks whether entity is a part of this current entity at any
     * level of children.
     * @param e
     * @return
     */
    protected boolean isEntityPartOfThis(Entity e) {
    	if(e == null) return false;
    	if(e == this) return true;
        if(e.getParentEntity() == this) return true;
    	if(e.getParentEntity() != null && e.getParentEntity().getParentEntity() != null &&
    				e.getParentEntity().getParentEntity() == this) return true;
    	return false;
    }
    
}
