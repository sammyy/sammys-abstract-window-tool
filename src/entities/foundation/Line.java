/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.foundation;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import resources.data.containers.Dimensions;
import resources.data.containers.LineMath;
import resources.data.containers.Location;
import resources.exceptions.MultipleIntersectionsPointsForLineException;
import resources.exceptions.PerpendicularLineException;
import utilities.MathAlgebra;
import utilities.abstracts.Shape2D;
import utilities.interfaces.Childable;

/**
 * Line to be drawn on canvas if needed and collision.
 * Collision below refers to collision being below the line visually.
 * @author Sammyy
 */
public class Line extends Shape2D{

    private Location beginPath = null;
    private Location lineToA = null;
    
    private List<Line> additionalLines = new ArrayList<>();

    /**
     * Whether being below the line causes collision or
     * above it does.
     */
    private boolean collisionBelow = true;
    
    /**
     * 
     * @param a Point a
     * @param b Point b
     * @param collisionBelow Whether collision happens above or below line.
     * Below meaning where some Y is larger than line's Y.
     */
    public Line(String id, Location a, Location b, boolean collisionBelow) {
        super("");
        super.setDimensions(new Dimensions(0+"",0+""));
        this.collisionBelow = collisionBelow;
        initAll(a, b);
    }
    
    /**
     * 
     * @param a Point a
     * @param b Point b
     * @param collisionBelow Whether collision happens above or below line.
     */
    public Line(Location a, Location b, boolean collisionBelow) {
        super("");
        super.setDimensions(new Dimensions(0+"",0+""));
        this.collisionBelow = collisionBelow;
        initAll(a, b);
    }
    
    public boolean getCollisionBelow(){
        return this.collisionBelow;
    }
    
    @Override
    public void initAll(Location a, Location b) { 
        super.setA(a);
        super.setB(b);
        this.beginPath = a;
        this.lineToA = b;
        setLocation(new Location(0,0));//Needed
    }
    
    @Override
    public boolean isInside(int x, int y) {
        Location rightPoint = Location.getBiggestX(this.getA(), this.getB()).add(this.getLocation());
        Location leftPoint = Location.getSmallestX(this.getA(), this.getB()).add(this.getLocation());
        Location diff = rightPoint.subtract(leftPoint);
      
        float incline = diff.getY() / (float)diff.getX();
        int m = (int) Math.round(rightPoint.getY() - rightPoint.getX() * incline);
        int maxYAllowed;

        if (x >= leftPoint.getX() && x <= rightPoint.getX()) {
            maxYAllowed = (int) Math.round(incline*x + m);
            if (this.collisionBelow) {
                if(y >= maxYAllowed && y < maxYAllowed+2){//The canvas reverses ycoords
                    return true;
                }
            } else {
                if(y <= maxYAllowed && y > maxYAllowed-2){//The canvas reverses ycoords
                    return true;
                } 
            }
        }
        return  this.isInsideAdditionalLines(x, y);
    }
    
    /**
     * Check whether x and y is inside the collidable area
     * in any additional lines besides the first line(obligatory).
     * @param x
     * @param y
     * @return 
     */
    private boolean isInsideAdditionalLines(int x , int y){
        for(Entity e: this.additionalLines){
            if(e instanceof Line && ((Line) e).isInside(x, y)){
                return true;
            }
        }
        return false;
    }

    public boolean hasCollided(Entity collidedWith) {
        Location feet = collidedWith.getBottomLocation();
        Location head = collidedWith.getHeadLocation();
        int entityWidth = collidedWith.getRealDimensions().getWidth();
        boolean collided = false;

        int yMin = feet.getY() - collidedWith.getDepth();
        int xMax = head.getX()+entityWidth;
        for (int y = feet.getY(); yMin < y; y--) {
            for (int x = head.getX(); x < xMax; x++) {
                collided = this.isInside(x, y);
                if (collided == true) {
                    return collided;
                }
            }
        }
        return collided;
    }

    
    /**
     * Add additional lines to collide with
     * An additional line acts as a MultiLine.
     * It is the same line but extended.
     * @param line
     * @return 
     */
    public void addAdditionalLine(Line line){
    	//Important for location
    	line.setParentEntity(this);
    	line.setBrightness(this.getBrightness());
        this.additionalLines.add(line);
    }
    
    public List<Entity> getOwnedEntities() {
    	return Collections.unmodifiableList(this.additionalLines);
    }
    
    /**
     * {@inheritDoc }
     * @param decimal 
     */
    @Override
    public void scale(float decimal){
        super.scale(decimal);
        for(Entity e: this.additionalLines){
            ((Line) e).scale(decimal);
        }
    }

    
    @Override
    public String getEncodedExtraData(){
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        sb.append(super.getEncodedLocation(this.beginPath));
        sb.append(super.getEncodedLocation(this.lineToA));
        return sb.toString();
    }
    
    /**
     * Sets brightness for main line and secondary
     * lines for collision.
     * {@link #setBrightness(int)}
     */
    @Override
    protected void setBrightness(int amount) {
    	super.setBrightness(amount);
    	for(Entity e: this.getChildEntities()) {
    		e.setBrightness(amount);
    	}
    }
    
    public List<Location> intersectsWith(Shape2D shape) 
    		throws MultipleIntersectionsPointsForLineException{
    	List<Location> locs = new ArrayList<>();
    	if(shape instanceof Line) {
    		if(((Line) shape).isNonExistant()) return null;
    		locs.add(this.intersects((Line) shape));
    		for(Entity es: this.additionalLines) {
    			if(es instanceof Line) {
    				Line les = (Line) es;
    				locs.add(les.intersects((Line) shape));
    			}
    		}
    	} else if(shape instanceof Rectangle) {
    		
    	} else if(shape instanceof Ellips) {
    		
    	}
    	return Location.getDistinctLocations(locs);
    }
    
    /**
     * Returns where the two lines intersect or null.
     * If they theoretically intersect at a decimal point then
     * a rounded location is returned.
     * @param b
     * @return Location where intersection happened or null.
     */
    private Location intersects(Line b) 
    		throws MultipleIntersectionsPointsForLineException{
    	LineMath lineMath = new LineMath(this);
        LineMath lineMathB = new LineMath(b);
        
        if(lineMath.isPerp()) return this.intersects(b, this);
        if(lineMathB.isPerp()) return this.intersects(this, b);
        
        if(MathAlgebra.xIntervalIntersects(lineMath.getLeftPoint(),lineMath.getRightPoint(), 
        		lineMathB.getLeftPoint(), lineMathB.getRightPoint())) {
        	//X intervals intersects between lines => potential line intersection
        	float x = ((lineMathB.getM() - lineMath.getM()) / 
        			(lineMath.getIncline() - lineMathB.getIncline()));
        	if(Math.round(x * lineMath.getIncline() + lineMath.getM()) == 
        			Math.round(x * lineMathB.getIncline() + lineMathB.getM())) {
        	    x = Math.round(x);
        		if(!(x >= lineMath.getLeftPoint().getX() && 
        				x <= lineMath.getRightPoint().getX())) return null;
        		if(!(x >= lineMathB.getLeftPoint().getX() && 
        				x <= lineMathB.getRightPoint().getX())) return null;
        		return new Location((int) x, 
        				(int)(x * lineMath.getIncline() + lineMath.getM()));
        	}
        }
        return null;
    }
    
    /**
     * Return a point of intersection between a line and a perpendicular line.
     * If {@link line}
     * @param line
     * @param perpendicular
     * @return
     * @throws MultipleIntersectionsPointsForLineException
     */
	private Location intersects(Line line, Line perpendicular)
			throws MultipleIntersectionsPointsForLineException {
		LineMath perp = new LineMath(perpendicular);
		if (perp.isPerp()) {
			LineMath lineMath = new LineMath(line);
			if (lineMath.isPerp()) {
				// Both lines perpendicular
				if(MathAlgebra.yIntervalIntersects(lineMath.getLeftPoint(), 
						lineMath.getRightPoint(), perp.getLeftPoint(),
						perp.getRightPoint(), false) 
						&& line.getA().getX() == perpendicular.getA().getX())
				throw new MultipleIntersectionsPointsForLineException();
			} else {

				// Check if there is a common x for both Lines
				if (MathAlgebra.xIntervalIntersects(perp.getLeftPoint(), 
						perp.getLeftPoint(), lineMath.getLeftPoint(),
						lineMath.getRightPoint())) {
					int x = perp.getLeftPoint().getX();
					// Line's y point where the two lines may intersect
					int lineY = (int) (lineMath.getIncline() * x + lineMath.getM());

					if (lineY <= perp.getRightPoint().getY() && lineY >= perp.getLeftPoint().getY()) {
						return new Location(x, lineY);
					}
				} else {
					// If x intervals doesn't collide there is no possibility
					// for intersection
					return null;
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns whether Location A and B making up this Line
	 * are equal. If so, then the Line is non-existant.
	 * @return
	 */
	public boolean isNonExistant() {
		LineMath lineMath = new LineMath(this);
		if(lineMath.isPerp() && lineMath.getDiff().getY() == 0) {
			//If a line isn't actually a line
			//This prevents Stackoverflow as this would always be
			//a perpendicular line. Even if rotated.
			return true;
		}
		return false;
	}
    
    /**
     * Used for testing that lines correctly finds intersecting points also
     * for cases of irregular lines which are perpendicular and don't
     * follow LineMath.
     * @author Sammyy
     *
     */
    public static class Test{
    	/**
    	 * null (0,0) (0, 0) null (-1, -1) null MultipleExc. null
    	 * Test and WORKS
    	 */
    	public static void testIntersects() {
    		Line a = null;
    		Line b = null;
			try {
				a = new Line("", new Location(-1, 1), new Location(1, -1), false);
				b = new Line("", new Location(-1, -1), new Location(-2, -1), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(1, -1), false);
				b = new Line("", new Location(-1, -1), new Location(1, 1), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(1, -1), false);
				b = new Line("", new Location(-1, -1), new Location(1, 2), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(-1, -1), false);
				b = new Line("", new Location(1, -1), new Location(1, 1), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleIntersectionExc.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(-1, -1), false);
				b = new Line("", new Location(-1, -1), new Location(1, 1), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleIntersectionExc.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(-1, -1), false);
				b = new Line("", new Location(-1, -3), new Location(-1, -2), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleIntersectionExc.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		try {
				a = new Line("", new Location(-1, 1), new Location(-1, -1), false);
				b = new Line("", new Location(-1, -1), new Location(-1, -2), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleIntersectionExc.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		try {
				a = new Line("", new Location(-1, 1), new Location(-1, -1), false);
				b = new Line("", new Location(-2, -1), new Location(-2, -1), false);
	    		System.out.println(a.intersectsWith(b));
			} catch (MultipleIntersectionsPointsForLineException e) {
				System.out.println("MultipleIntersectionExc.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    /**
     * Return a new Line which is rotated 90 degrees
     * Can be used to make a line usable with LineMath class.
     * @return
     */
    public Line getRotatedLine90() {
    	Location a = new Location(-this.getA().getY(), this.getA().getX());
    	Location b = new Location(-this.getB().getY(), this.getB().getX());
    	return new Line(a, b, this.collisionBelow);
    }

    
}
