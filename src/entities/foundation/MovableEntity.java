/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.foundation;

import resources.data.containers.Location;
import utilities.interfaces.Movable;

/**
 *
 * @author Sammyy
 */
public abstract class MovableEntity extends Entity implements Movable<Location>{
    
    public MovableEntity(String data){
        super(data);
    }
    
}
