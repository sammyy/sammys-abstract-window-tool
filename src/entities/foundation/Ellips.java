/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.foundation;

import utilities.abstracts.Shape2D;

import java.util.ArrayList;
import java.util.List;

import resources.data.containers.Dimensions;
import resources.data.containers.LineMath;
import resources.data.containers.Location;
import utilities.interfaces.Childable;

/**
 * Represent an Ellips. Also calculates intersection or collision.
 * When calculating intersection no float values are the end result.
 * Therefore the midpoint of a circle/origin has to be Int values.
 * Therefore locations input into constructor will be modified as needed
 * to make midpoint an actual Int, same for radius.
 * Eg. input ((0, 0), (3, 3)) has origin (1.5, 1.5) 
 *       - > ((0, 0), (4, 4)) has origin (2, 2)
 * @author Sammyy
 */
public class Ellips extends Shape2D implements Childable{

    private int radiusX = 0;//TODO fix int vs float
    private int radiusY = 0;
    
    public Ellips(Location a, Location b){
        super("");
        super.setDimensions(new Dimensions(0+"",0+""));
        initAll(a, b);
    }
    
    @Override
    public void initAll(Location a, Location b){
                
    	//Make origin even point for calculations to be correct
    	if((a.getX() - b.getX()) % 2 != 0) a = a.add(new Location(1, 0));
    	if((a.getY() - b.getY()) % 2 != 0) b = b.add(new Location(0, 1));
    	
        super.setA(a);
        super.setB(b);
        
        Location difference = Location.getLeftUpperCorner(a, b).
                subtract(Location.getRightLowerCorner(a, b));
        
        Location lCorner = Location.getLeftUpperCorner(a, b);
        
        radiusX = Math.abs(difference.getX()/2);
        radiusY = Math.abs(difference.getY()/2);
         super.setLocation(new Location(0,0));//Needed
    }
    
    private Location getOrigin() {
    	Location a = this.getA();
    	Location b = this.getB();
    	Location difference = Location.getLeftUpperCorner(a, b).
                  subtract(Location.getRightLowerCorner(a, b));
          
        Location lCorner = Location.getLeftUpperCorner(a, b);
        lCorner = lCorner.add(this.getLocation());
        Location origin = lCorner.add(new Location(
                  Math.abs(difference.getX()/2), 
                  Math.abs(difference.getY()/2)));
        return origin;
    }
    
    private Location getRelativeOrigin() {
    	return this.getOrigin().subtract(this.getLocation());
    }
    

    @Override
    public boolean isInside(int x, int y) {
        double partA = Math.pow(x-getOrigin().add(this.getLocation()).getX(), 2)/Math.pow(radiusX, 2);
        double partB = Math.pow(y-getOrigin().add(this.getLocation()).getY(), 2)/Math.pow(radiusY, 2);
        return (partA+partB <= 1);
    }

    public boolean hasCollided(Entity collidedWith) {
        Location feet = collidedWith.getBottomLocation();
        Location head = collidedWith.getHeadLocation();
        int entityWidth = Integer.valueOf(collidedWith.getRealDimensions().getWidth());
        boolean collided = false;
        
        int yMin = feet.getY() - collidedWith.getDepth();
        int xMax = head.getX()+entityWidth;
        for (int y = yMin; y < feet.getY(); y++) {
            for (int x = head.getX(); x < xMax; x++) {
                collided = this.isInside(x, y);
                if (collided == true) {
                    return collided;
                }
            }
        }
        return collided;
    }
    /**
     * Used by parent to move this shape.
     * @param loc
     */
    @Override
    public void setLocation(Location loc) {
        super.setLocation(loc);
    }
    
    @Override
    public String getEncodedExtraData(){
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        sb.append(super.getEncodedLocation(new Location(this.radiusX, this.radiusY)));
        sb.append(super.getEncodedLocation(this.getRelativeOrigin()));
        return sb.toString();
    }

	@Override
	public List<Location> intersectsWith(Shape2D shape) {
		List<Location> locs = new ArrayList<>();
		if(shape instanceof Line) {
			try {
				if(((Line) shape).isNonExistant()) return null;
				for(Entity e: shape.getChildEntities()) {
	    			if(e instanceof Line) {
	    				locs.addAll( this.intersectsWithLine((Line) e));
	    			}
	    		}
	    		locs.addAll(this.intersectsWithLine((Line) shape));
			} catch (NullPointerException e) {
				
			}
    		return locs;
		}
		return null;
	}
	
	
	private List<Location> intersectsWithLine(Line line) {
		LineMath lineMath = new LineMath(line);
		
		if(lineMath.isPerp()) {
			//Rotate Ellips and Circle and calculate
			Ellips rotated = this.getRotatedEllips90();
			List<Location> intLocs = 
					rotated.intersectsWith(line.getRotatedLine90());
			//Rotate back results
			return (intLocs == null) ? null : Location.getRotatedLocationsSub90(intLocs);
		}
		
		float p = this.getPOfIntersectionEquation(lineMath);
		float q = this.getQOfIntersectionEquation(lineMath);
		double partToSqrt = Math.pow(p, 2) / 4 - q;
		
		double x1 = (-p / 2) + Math.sqrt(partToSqrt); 
		double x2 = (-p / 2) - Math.sqrt(partToSqrt);
		//Imaginary number = no solution/intersection
		if(Double.isNaN(x1)) return null;

		List<Location> locs = new ArrayList<>();
		if(isCoordsOnEllips(x1, lineMath.getY((float) x1)) 
				&& x1 >= lineMath.getLeftPoint().getX() 
				&& x1 <= lineMath.getRightPoint().getX()) {
			locs.add(
					new Location(
							(int) Math.round(x1), 
							(int) Math.round(lineMath.getY((float) x1)) ));
		}
		if(isCoordsOnEllips(x2, lineMath.getY((float) x2))
				&& x2 >= lineMath.getLeftPoint().getX() 
				&& x2 <= lineMath.getRightPoint().getX()) {
			locs.add(
					new Location(
							(int) Math.round(x2), 
							(int) Math.round(lineMath.getY((float) x2)) ));
		}
		return locs;
	}
	
	/**
	 * Return true if coordinates exist on the mathematical expression
	 * of the Ellips.
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean isCoordsOnEllips(double x, double y) {
		double a = Math.pow(x - getOrigin().getX(), 2) / Math.pow(this.radiusX, 2);
		double b = Math.pow(y - getOrigin().getY(), 2) / Math.pow(this.radiusY, 2);
		return (1 == Math.round(a+b));
	}
	
	/**
	 * Part of equation to find intersection between Line
	 * and Ellips.
	 * @param lineMath
	 * @return
	 */
	private float getROfIntersectionEquation(LineMath lineMath) {
		float a = (float) (2 * Math.pow(this.radiusY, 2) 
				* getOrigin().getX());
		float b = (float) (2 * lineMath.getIncline() * 
				lineMath.getM() * Math.pow(this.radiusX, 2));
		float c = (float) (2 * lineMath.getIncline() * 
				getOrigin().getY() * Math.pow(this.radiusX, 2));
		return a - b + c;
	}
	
	/**
	 * Part of equation to find intersection between Line
	 * and Ellips.
	 * @param lineMath
	 * @return
	 */
	private float getSOfIntersectionEquation(LineMath lineMath) {
		return (float) 
				( -Math.pow(this.radiusY, 2) 
						-Math.pow(lineMath.getIncline(), 2) 
						* Math.pow(this.radiusX, 2));
	}
	
	/**
	 * Part of equation to find intersection between Line
	 * and Ellips.
	 * @param lineMath
	 * @return
	 */
	private float getNOfIntersectionEquation(LineMath lineMath) {
		float a = (float) (- Math.pow(lineMath.getM(), 2) + 2 * lineMath.getM() * getOrigin().getY());
		float b = (float) (- Math.pow(this.radiusY * getOrigin().getX(), 2) / Math.pow(this.radiusX, 2));
		float c = (float) (- Math.pow(getOrigin().getY(), 2) + Math.pow(this.radiusY, 2));
		return a + b + c;
	}
	
	private float getPOfIntersectionEquation(LineMath lineMath) {
		return this.getROfIntersectionEquation(lineMath) 
				/ this.getSOfIntersectionEquation(lineMath);
	}
	
	private float getQOfIntersectionEquation(LineMath lineMath) {
		return (float) 
				(this.getNOfIntersectionEquation(lineMath) 
						* Math.pow(this.radiusX, 2) 
				/ this.getSOfIntersectionEquation(lineMath));
	}
	
	public static class Test{
		/**
		 * null
		 * null
		 * (0, 0) & (1, 1)
		 * (1, 1)
		 * (1, 1) & (3, 3)
		 * (2, 0) & (2, 4) - rotated case 
		 */
		public static void test() {
			Ellips e; Line l; List<Location> locs;
			try {
				e = new Ellips(new Location(-2, 2), new Location(2, -2));
				l = new Line(new Location(-1, -1), new Location(1, 1), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				e = new Ellips(new Location(-1, 1), new Location(1, -1));
				l = new Line(new Location(-3, -1), new Location(-2, 1), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				e = new Ellips(new Location(0, 1), new Location(2, -1));
				l = new Line(new Location(-1, -1), new Location(1, 1), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				e = new Ellips(new Location(0, 4), new Location(4, 0));
				l = new Line(new Location(-1, -1), new Location(1, 1), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				e = new Ellips(new Location(0, 4), new Location(4, 0));
				l = new Line(new Location(-1, -1), new Location(4, 4), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				e = new Ellips(new Location(0, 4), new Location(4, 0));
				l = new Line(new Location(2, 0), new Location(2, 4), false);
				locs = e.intersectsWithLine(l);
				if(locs != null && !locs.isEmpty()) {
					for(Location loc: locs) {
						System.out.println(loc);
					}
					System.out.println("--------------");
				} else { System.out.println("null"); }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
    /**
     * Return a new Ellips which is rotated 90 degrees
     * @return
     */
	public Ellips getRotatedEllips90() {
		Location a = new Location(-this.getA().getY(), this.getA().getX());
    	Location b = new Location(-this.getB().getY(), this.getB().getX());
    	return new Ellips(a, b);
	}
    
    
}
