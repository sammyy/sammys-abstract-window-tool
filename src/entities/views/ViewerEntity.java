/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views;

import entities.foundation.Entity;
import entities.views.children.Button;
import entities.views.children.LoginInputField;
import entities.views.children.ProtectedLoginInputField;
import entities.views.children.Text;
import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.EntityClientInfo;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.interfaces.ClickListener;
import utilities.interfaces.IOperator;
import utilities.interfaces.MouseMoveListener;

/**
 * A View that can be shown to the Viewer(user etc.). A View
 * can contain: text, inputfields, buttons etc,
 * @author Sammyy
 */
public abstract class ViewerEntity extends StaticEntity implements ClickListener<ClickData>, MouseMoveListener<Location>{
    /**
     * Operator of this View. Either Guest or Player
     */
    protected IOperator operatorOfView = null;
    
    private int fontSize = 28;

	private String fontName = "Arial";
    private String fontColor = "#000000";

    public ViewerEntity(IOperator op, String data) {
        super(data);
        this.operatorOfView = op;
    }
    
    public ViewerEntity(IOperator op, String data, 
            Dimensions dim, Location loc) {
        super(data, dim, loc);
        this.operatorOfView = op;
    }
    
    @Override
    public void setDimensions(Dimensions dim){
        super.setDimensions(dim);
        this.alignChildren();//Re-align children after changing dimensions
    }
    
    @Override
    public void setLocation(Location loc){
        super.setLocation(loc);
    }

    /**
     * Adds LoginInputFields to children.
     * @param name 
     */
    protected void addInputField(String name) {
        EntityClientInfo eci;
        LoginInputField lif;

        lif = new LoginInputField(this.operatorOfView, LoginInputField.DEFAULT_IMG_ID,
                new Dimensions(LoginInputField.DEFAULT_WIDTH, LoginInputField.DEFAULT_HEIGHT),
                new Location(0,0));
        
        this.addChildEntity(lif);
      
    }
    
    protected void addProtectedInputField(String name){
        EntityClientInfo eci;
        LoginInputField lif;

        lif = new ProtectedLoginInputField(this.operatorOfView, LoginInputField.DEFAULT_IMG_ID,
                new Dimensions(LoginInputField.DEFAULT_WIDTH, LoginInputField.DEFAULT_HEIGHT),
                new Location(0,0));
        
        this.addChildEntity(lif);
    }
    
     /**
     * Adds a textelement to Entity.
     * @param name 
     * @param id
     * @param text
     * @param maxWidth max allowed width of text
     * @param fontName
     * @param fontSize
     * @param fontColor
     */
    protected void addText(String text, 
            int maxWidth, String fontName, 
            String fontSize, String fontColor) {
  
        Text txt = new Text(this.operatorOfView, text,
                new Dimensions(maxWidth+"", fontName, fontSize, fontColor),
                new Location(0,0));

        this.addChildEntity(txt);
      
    }
    
    /**
     * Adds a textelement to Entity. With default font color set as black.
     * @param name 
     * @param id
     * @param text
     * @param maxWidth max allowed width of text
     * @param fontName
     * @param fontSize
     */
    protected void addText(String text, 
            int maxWidth, String fontName, String fontSize) {
    	this.addText(text, maxWidth, fontName, fontSize, "#000000");
    }
    
     /**
     * Adds a textelement to Entity. Maxwidth is text width.
     * @param name 
     * @param id
     * @param text
     * @param fontName
     * @param fontSize
     */
    protected void addText(String text, 
            String fontName, String fontSize) {
        this.addText(text, EntityCalculator.getBoundsOfText(text, fontName, fontSize)[0],  fontName, fontSize);
    }
    
    /**
     * Adds a textelement to Entity. Maxwidth is text width.
     * @param name 
     * @param id
     * @param text
     * @param fontName
     * @param fontSize
     */
    protected void addText(String text, 
            String fontName, String fontSize, String fontColor) {
        this.addText(text, EntityCalculator.getBoundsOfText(text, fontName, fontSize)[0],  fontName, fontSize, fontColor);
    }
    
    /**
     * Adds a button to the entities childList.
     * @param name 
     * @param text Button's text
     * @param viewOnClick entity viewed on click
     * @param removeOnClick entity removed on click
     */
    protected void addButton(String name, String buttonText, 
            ViewerEntity viewOnClick, ViewerEntity removeOnClick) {
        this.addButton(name, buttonText, viewOnClick, removeOnClick, false);
      
    }
    
     /**
     * Adds a button to the entities childList.
     * @param name 
     * @param text Button's text
     * @param viewOnClick entity viewed on click
     * @param removeOnClick entity removed on click
     * @param waitUpdate Whether to wait sending update to client.
     */
    protected void addButton(String name, String buttonText, 
            ViewerEntity viewOnClick, ViewerEntity removeOnClick, boolean waitUpdate) {
        EntityClientInfo eci;
        Button button;

        button = new Button(this.operatorOfView, Button.DEFAULT_IMG_ID,
                new Dimensions(Button.DEFAULT_WIDTH, Button.DEFAULT_HEIGHT),
                new Location(0,0), buttonText, viewOnClick, removeOnClick);
        
        this.addChildEntity(button, waitUpdate);
      
    }
    
    /**
    * Adds a button to the entities childList.
    * @param name 
    * @param buttonText Button's text
    * @param viewOnClick entity viewed on click
    * @param removeOnClick entity removed on click
    */
   protected void addButton(String name, String buttonText, 
           ViewerEntity viewOnClick, ViewerEntity removeOnClick, 
           int width, int height) {
	   this.addButton(name, buttonText, viewOnClick, removeOnClick, width, height, false);
   }
    
     /**
     * Adds a button to the entities childList.
     * @param name 
     * @param buttonText Button's text
     * @param viewOnClick entity viewed on click
     * @param removeOnClick entity removed on click
     * @param waitUpdate Whether to wait sending update to client.
     */
    protected void addButton(String name, String buttonText, 
            ViewerEntity viewOnClick, ViewerEntity removeOnClick, 
            int width, int height, boolean waitUpdate) {
        EntityClientInfo eci;
        Button button;

        button = new Button(this.operatorOfView, Button.DEFAULT_IMG_ID,
                new Dimensions(Button.DEFAULT_WIDTH, Button.DEFAULT_HEIGHT),
                new Location(0,0), buttonText, viewOnClick, removeOnClick);
        
        button.setFont("Arial", height-6);
        
        this.addChildEntity(button, waitUpdate);
      
    }
    /**
     * Adds a button to ViewerEntity using an Id to locate the buttons
     * data in HuppiResourceReader. No text is added on button.
     * @param id
     * @param name 
     */
    protected void addButtonWithId() {

        Button button = new Button(this.operatorOfView, Button.DEFAULT_IMG_ID,
                new Dimensions(Button.DEFAULT_WIDTH, Button.DEFAULT_HEIGHT),
                new Location(0,0), "", null, null);

        this.addChildEntity(button);
    }
    
    /**
     * Used to align children to parent, after parent is positioned.
     */
    public abstract void alignChildren();
    
    @Override
    public ViewerEntity getLastAddedChild(){
        Entity e = super.getLastAddedChild();
        if(e instanceof ViewerEntity){
            return (ViewerEntity) e;
        }
        return null;
    }
    
    /**
     * BUG: not working
     */
    @Override
	public boolean isVisible(){
    	if(this.operatorOfView == null) return false;
        return this.operatorOfView.hasOverlayEntity(this);
    }
    
    public void setFont(String fontName, int fontSize, String fontColor){
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.reinitiateText();
    }
    
    public void setFont(String fontName, int fontSize){
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.reinitiateText();
    }
    
    public void setFont(Dimensions dim) {
    	this.fontName = dim.getFontName();
    	this.fontSize = Integer.valueOf(dim.getFontSize());
    	this.fontColor = dim.getFontColor();
    }
    
    public int getFontSize() {
		return fontSize;
	}

	public String getFontName() {
		return fontName;
	}

	public String getFontColor() {
		return fontColor;
	}
    
    /**
     * Called when font properties is update
     * @return 
     */
    protected abstract void reinitiateText();
    
    /**
     * Close this viewer
     */
    protected void close() {
    	if(this.operatorOfView != null) {
    		this.operatorOfView.removeOverlayEntity(this);
    	}
    }
    
    public IOperator getOperator(){
        return this.operatorOfView;
    }
    
}
