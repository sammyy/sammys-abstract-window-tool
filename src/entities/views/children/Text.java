/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views.children;

import entities.foundation.Entity;
import entities.views.ViewerEntity;
import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.handlers.HandlersContainer;
import utilities.interfaces.Childable;
import utilities.interfaces.IOperator;
/**
 * Single row of text.
 * @author Sammyy
 */
public class Text extends ViewerEntity implements Childable{

	public static final String DEFAULT_IMG_ID = "Arial";
	public static final int DEFAULT_SIZE = 16;
    
	/**
	 * 
	 * @param op Not needed
	 * @param text Text
	 * @param dim Dimensions of text
	 * @param loc
	 */
    public Text(IOperator op, String text, 
            Dimensions dim, Location loc) {
        super(op, text, dim, loc);
    }

       @Override
    public void alignChildren() {
        //nothing
    }

    @Override
    public void setLocation(Location loc) {
        super.setLocation(loc);
    }

	@Override
	protected void reinitiateText() {
		// Nothing
	}
	
	public void setText(String data) {
		super.setData(data);
	}
	
	public Integer getTextHeight() {
		Dimensions dim = this.getDimensions();
		String fontName = dim.getFontName();
		String fontSize = dim.getFontSize() + "";
		if(fontName == null || fontSize == null) {
			throw new Error("Text dimensions not defined for text.");
		}
		return EntityCalculator.getBoundsOfText(this.getData(), fontName, fontSize)[1];
	}

	@Override
	public void handleClickData(ClickData data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleMouseMoveData(Location mData) {
		// TODO Auto-generated method stub
		
	}
    
}
