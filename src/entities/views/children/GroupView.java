package entities.views.children;

import java.util.ArrayList;
import java.util.List;

import entities.foundation.Entity;
import entities.views.ViewerEntity;
import entities.views.children.Button;
import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.interfaces.ClickListener;
import utilities.interfaces.IOperator;

/**
 * Group of elements inside a transparent view.<br>
 * By default: Centrates the elements in a row upon using {@link #alignChildren()} <br>
 * May override method or position elements independently instead.<br>
 * @author Sammyy
 *
 */
public class GroupView extends ViewerEntity implements ClickListener<ClickData>{

	public GroupView(IOperator op, int width, int height) {
		super(op, "9021");//9021 => Transparent image
		this.setDimensions(new Dimensions(width, height));
	}

	@Override
	public void handleClickData(ClickData data) {
		ViewerEntity clickedChild = null;
		/**
		 * Handle clicks on childentities
		 */
		List<Entity> children = this.getChildEntities();
		if(children != null && data != null && data.getLocation() != null) {
			for(Entity child: children) {
				if(child instanceof ViewerEntity) {
					ViewerEntity viewChild = (ViewerEntity) child;
					if(EntityCalculator.isEntityAreaClicked(data.getLocation(), viewChild)) {
						clickedChild = viewChild;
					}
				}
			}
		}
		
		if(clickedChild != null) {
			if(clickedChild instanceof Button) {//Click child button
				Button button = (Button) clickedChild;
				button.clickButton();
			} else {//Send click data to clicked child which isn't button
				clickedChild.handleClickData(data);
			}
		}
	}

	@Override
	public void handleMouseMoveData(Location mData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alignChildren() {
		int spaceBetweenEach = 15; //px
		// Centrtate children in a row
		List<Entity> children = new ArrayList<>(this.getChildEntities());
		List<Location> locs = EntityCalculator.getCentratedAlignedRow(children, spaceBetweenEach, this);
		int i = 0;
		for (Location placeAt : locs) {
			ViewerEntity child = (ViewerEntity) children.get(i++);
			
			child.setLocation(placeAt);// Place at its position
		}
	}

	@Override
	protected void reinitiateText() {
		// TODO Auto-generated method stub
		
	}
	
	public void addChildEntity(ViewerEntity entity) {
		super.addChildEntity(entity);
	}

}
