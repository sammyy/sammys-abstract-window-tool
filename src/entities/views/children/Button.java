/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views.children;


import java.util.ArrayList;
import java.util.List;

import entities.views.ViewerEntity;
import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import resources.data.containers.UpdateStatus;
import sawt.views.tools.TextMaker;
import utilities.EntityCalculator;
import utilities.interfaces.Childable;
import utilities.interfaces.IForm;
import utilities.interfaces.IOperator;

/**
 *
 * @author Sammyy
 */
public class Button extends ViewerEntity implements Childable{
	
	public static final String DEFAULT_IMG_ID = "Button";
	public static final int DEFAULT_WIDTH = 250;
	public static final int DEFAULT_HEIGHT = 85;
    
    private ViewerEntity viewOnClick = null;
    private ViewerEntity removeOnClick = null;
    private IForm callbackOnClick = null;

    private Text buttonText = null;

    public Button(IOperator op, String data, 
            String buttonText,
            ViewerEntity viewOnClick, ViewerEntity removeOnClick) {
        super(op, data);
        this.initButton(buttonText, viewOnClick, removeOnClick);
    }
    
    public Button(IOperator op, String data, 
            Dimensions dim, Location loc,
            String buttonText,ViewerEntity viewOnClick, 
            ViewerEntity removeOnClick) {
        super(op, data, dim, loc);
        this.initButton(buttonText, viewOnClick, removeOnClick);
    }
    
    private void initButton(String buttonText, ViewerEntity viewOnClick,
            ViewerEntity removeOnClick){
        this.initButtonText(buttonText);
        this.viewOnClick = viewOnClick;
        this.removeOnClick = removeOnClick;
    }
    
    private void initButtonText(String buttonText){
        if(buttonText == null || buttonText.isEmpty()) return;
        
        TextMaker textMkr = new TextMaker(buttonText, this.getFontName(), this.getFontColor(), this.getFontSize());
        Text text = textMkr.getText();
        this.addChildEntity(text);
        
        this.buttonText = text;
        this.alignChildren();
    }
    /**
     * Set callback entity for IForm objects that need to know when
     * button is clicked.
     * @param form 
     */
    public <T extends IForm> void setCallbackEntity(T form){
        this.callbackOnClick = form;
    }
    
    @Override
    public void alignChildren() {
    	if(this.buttonText == null) return;
    	
        int xOffset = Math.abs(this.getDimensions().getWidth() - this.buttonText.getDimensions().getWidth()) / 2;
        int yOffset = Math.abs(this.getDimensions().getHeight() - this.buttonText.getTextHeight()) / 2;

        this.buttonText.setLocation(new Location(xOffset, yOffset));
    }

     /**
     * Click button and view given viewer. Also
     * remove viewer if specified.
     */
    public void clickButton(){
    	boolean setToVisibleNow = false;
        ViewerEntity e = viewOnClick;
        if(e != null){
            e.alignChildren();
			if (!this.operatorOfView.hasOverlayEntity(e)) {
				this.operatorOfView.addOverlayUpdate(e, UpdateStatus.UPDATE);
				setToVisibleNow = true;
			}
        }    

        if (this.removeOnClick != null && !setToVisibleNow) {
            this.operatorOfView.removeOverlayEntity(removeOnClick);
        }

        if(this.callbackOnClick != null){
            this.callbackOnClick.onButtonClick(this.getUniqueId() + "");
        }
    }

	@Override
	protected void reinitiateText() {
		this.clearChildEntities(false);
		this.initButtonText(buttonText.getData());
	}

	@Override
	public void handleClickData(ClickData data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleMouseMoveData(Location mData) {
		// TODO Auto-generated method stub
		
	}
    
}
