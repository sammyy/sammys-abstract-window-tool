package entities.views.children;

import entities.foundation.Entity;
import entities.views.ViewerEntity;
import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.handlers.HandlersContainer;
import utilities.interfaces.ClickListener;
import utilities.interfaces.IForm;
import utilities.interfaces.IOperator;

/**
 * Clickable text.
 * @author Sammyy
 *
 */
public class ClickableText extends ViewerEntity implements ClickListener<ClickData>{

	private IForm iform = null;
	
	/**
	 * Called when text is clicked.
	 * @param iform
	 */
	public void setCallbackOnClick(IForm iform) {
		this.iform = iform;
	}

	public ClickableText(IOperator op, String data, Dimensions dim, Location loc) {
		super(op, data, dim, loc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleClickData(ClickData data) {
		if(this.iform != null) this.iform.onButtonClick(data.getLastClickedEntity().getUniqueId() + "");
	}

	@Override
	public void alignChildren() {
		//Nothing
	}

	@Override
	protected void reinitiateText() {
		//Nothing
	}

	@Override
	public void handleMouseMoveData(Location mData) {
		// TODO Auto-generated method stub
		
	}

}
