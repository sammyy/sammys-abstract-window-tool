/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views.children;

import java.util.List;

import entities.views.InputEntity;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.handlers.HandlersContainer;
import utilities.interfaces.Childable;
import utilities.interfaces.IOperator;

/**
 *
 * @author Sammyy
 */
public class LoginInputField extends InputEntity {

	public static final String DEFAULT_IMG_ID = "InputField";
	public static final int DEFAULT_WIDTH = 250;
	public static final int DEFAULT_HEIGHT = 70;
    
    public LoginInputField(IOperator op, String data, 
            Dimensions dim, Location loc) {
        super(op, data, dim, loc);
        this.setFontSettings("BlipNormal", 15);
    }
    
    @Override
    public void alignChildren() {
        Childable child;
        List<Location> locs;
        locs = EntityCalculator.getCentratedAlignedRow(getChildEntities(),
                5, this.getDimensions().getWidth(), this.getDimensions().getHeight(),
                0 + 5, 0);

        for (int i = 0; i < getChildEntities().size(); i++) {
            child = (Childable) getChildEntities().get(i);
            child.setLocation(locs.get(i));
        }
    }
    
    
   
}
