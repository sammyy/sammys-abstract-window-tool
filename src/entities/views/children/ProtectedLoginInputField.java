/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views.children;

import java.util.List;

import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.handlers.HandlersContainer;
import utilities.interfaces.Childable;
import utilities.interfaces.IOperator;

/**
 * Protected input field. Used for sensitive information.
 * @author Sammyy
 */
public class ProtectedLoginInputField extends LoginInputField{
    
    public ProtectedLoginInputField(IOperator op, String data, Dimensions dim, Location loc) {
        super(op, data, dim, loc);
    }
    
    @Override
    public String getInputTextViewed(){
        String str = super.getInputText();
        String protectedString = "";
        for(int i=0; i<str.length(); i++){
            protectedString += "*";
        }
        return protectedString;
    }
    
}
