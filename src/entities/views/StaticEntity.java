/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views;

import entities.foundation.Entity;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.interfaces.Placable;


/**
 * Static entities must be placed out by someone initially and
 * therefore implements Placable.
 * @author Sammyy
 */
public abstract class StaticEntity extends Entity implements Placable<Location>{

    public StaticEntity(String data){
        super(data);
    }
    
    public StaticEntity(String data,
            Dimensions dim, Location loc){
        super(data);
        this.setDimensions(dim);
        this.setLocation(loc);
    }

    @Override
    public void setLocation(Location loc) {
        super.setLocation(loc);
    }
}
