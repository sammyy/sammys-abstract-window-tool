/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.views;

import entities.foundation.Entity;
import entities.views.children.Text;

import java.util.List;

import resources.data.containers.ClickData;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import resources.data.containers.UpdateStatus;
import resources.dataHandlers.KeyInputHandler;
import utilities.EntityCalculator;
import utilities.handlers.HandlersContainer;
import utilities.interfaces.Childable;
import utilities.interfaces.IOperator;
import utilities.interfaces.Callback;
import utilities.interfaces.Retrievable;

/**
 * An entity used for retrieving keyboard input.
 * Allows for callback when enter is pressed by implementing InputSendable.
 * @author Sammyy
 */
public class InputEntity extends ViewerEntity implements Childable{
    
    private KeyInputHandler inputText;
    private Integer maxLetters = null;

	private Integer fakedWidth = null;
    private Integer fakedHeight = null;
    /**
     * Used to call subclass when enter is pressed.
     */
    private Callback callbackOnEnter = null;
    
    /**
     * Used to call subclass when input is entered.
     */
    private Callback callbackOnInput = null;
    
    /**
     * Overrides getInputText()
     */
    private Retrievable<String> inputOverrider = null;
    
    /**
     * KeyInputHandler to be merged with inputText
     */
	private KeyInputHandler kHandler;

	public InputEntity(IOperator op, String data) {
        super(op, data);
         initInput();
    }
    
    public InputEntity(IOperator op, String data, 
            Dimensions dim, Location loc) {
        super(op, data, dim, loc);
        initInput();
    }
    /**
     * Change inputEntities font settings from default ones.
     * @param fontName
     * @param fontSize 
     * @param fontColor - hex or name for css/canvas in grml
     */
    protected void setFontSettings(String fontName, int fontSize, String fontColor){
        super.setFont(fontName, fontSize, fontColor);
    }
    
    /**
     * Change inputEntities font settings from default ones. Uses font color black as default.
     * @param fontName
     * @param fontSize 
     */
    protected void setFontSettings(String fontName, int fontSize){
        super.setFont(fontName, fontSize);
    }
    
    /**
     * Allows you to specify maximum amount of letters accepted into
     * the InputEntity.
     * @param amount 
     */
    protected void setMaxLetters(Integer amount){
        this.maxLetters = amount;
    }
    /**
     * If the subclass should transfer gathered input
     * to a complete message when enter is pressed.
     */
    protected void transferGatheredInputOnEnter(){
        inputText.setActionOnEnter(true);
    }
    
    private void initInput(){
        inputText = new KeyInputHandler();
        inputText.setActionOnBackspace(true);
        inputText.setActionOnEnter(false);
    }
    /**
     * Whether input text has passed the maximum amount of letters
     * @return 
     */
    public boolean hasMaxLetters(){
        if(this.maxLetters != null && 
                this.maxLetters <= this.getInputText().length()){
            return true;
        }
        return false;
    }
    
    private boolean preventEnterBackSpace() {
    	return (this.getInputText().length() == 0 && 
    			(this.kHandler.isBackspacePressed() || this.kHandler.isEnterPressed()));
    }
    
    /**
     * Used to fetch new input/update the input
     */
    public void fetchInput(KeyInputHandler kHandler){
    	this.kHandler = kHandler;
        if(this.kHandler != null && (!this.hasMaxLetters() || 
                this.kHandler.isBackspacePressed()) && !preventEnterBackSpace()){
        	//Merge
            inputText.mergeKeyInputHandler(kHandler);
            super.clearChildEntities(false);//Clear previous text
            //Draw update text
            this.drawTextOntoInputEntity(inputText.getLengthGatheredInput());
          //Send callbacks
            if(this.callbackOnInput != null) this.callbackOnInput.callback();
            if(inputText.hasCompleteMessage() && callbackOnEnter != null){
                this.callbackOnEnter.callback();
            }
        }
    }
    
    private void drawTextOntoInputEntity(int inputLength) {
    	 if(inputLength > 0){
             List<Text> texts = EntityCalculator.
                     getTextEntitiesWithinBounds(this.getInputTextViewed(), 
                     		this.getFontName(), this.getFontSize()+"", this.getFontColor(), 
                             Integer.valueOf((this.fakedWidth != null) ? this.fakedWidth : this.getDimensions().getWidth()), 
                             Integer.valueOf((this.fakedHeight != null) ? this.fakedHeight : this.getDimensions().getHeight()));
             for(Text text: texts){
                 this.addChildEntity(text);
             }
             this.alignChildren();
             }
    }
    
    /**
     * Sets a faked dimensions for inputarea. Instead of actual dimensions of the
     * input entity's image.
     * @param width
     * @param height
     */
    protected void setFakedInputDimensions(int width, int height) {
    	this.fakedWidth = width;
    	this.fakedHeight = height;
    }
    
    public Integer getFakedWidth() {
		return fakedWidth;
	}

	public Integer getFakedHeight() {
		return fakedHeight;
	}

    /**
     * Clears previous values from inputhandler. This avoids filling new
     * inputfield with data from a previous one.
     */
    public void setFocused(){
        //Clear input
    	if(this.kHandler == null) return;
        this.kHandler.clearAllInput();
    }

    /**
     * Gathered input which is displayed in InputEntity.
     * @return 
     */
    public String getInputText() {
    	String gatheredInput = inputText.getGatheredInput();
    	gatheredInput = (this.inputOverrider == null) ? 
    			gatheredInput : this.inputOverrider.retrieve();
        return gatheredInput;
    }
    
    
    /**
     * Retrievable overrides getInputText()
     * @param inputOverrider
     */
    public void setInputOverrider(Retrievable<String> inputOverrider) {
		this.inputOverrider = inputOverrider;
        this.drawTextOntoInputEntity(1);
	}
    
    /**
     * If you specified to transfer gathered input
     * to a complete message on enter when you initialized
     * this object, you will receive the complete message here.
     * @return String|null
     */
    public String getCompleteInputText(){
        String complete = inputText.getCompleteMessage();
        return (complete.isEmpty()) ? null : complete;
    }
    
    /**
     * This is used to display text in inputfield.
     * For instance if you want password field you
     * can make this not show actual letters.
     * @return 
     */
    public String getInputTextViewed(){
        return this.getInputText();
    }

    @Override
    public void alignChildren() {
        Childable child;
        List<Location> locs;
        locs = EntityCalculator.getCentratedAlignedColumn(getChildEntities(),
                5, this);

        for (int i = 0; i < getChildEntities().size(); i++) {
            child = (Childable) getChildEntities().get(i);
            child.setLocation(locs.get(i));
        }
    }
    
    /**
     * This allows parent to keep entity relative to itself
     * @param loc 
     */
    @Override
    public void setLocation(Location loc){
        super.setLocation(loc);
    }

     /**
     * Set callback entity for when enter is pressed.
     * @param sendable
     */
    public void setCallbackEntity(Callback callback){
        this.callbackOnEnter = callback;
    }
    
    /**
     * Set callback entity for when adding new
     * input.
     * @param sendable
     */
    public void setCallbackEntityInputEvent(Callback callback){
        this.callbackOnInput = callback;
    }
    
    public void setActionOnEnter(boolean act) {
    	this.inputText.setActionOnEnter(act);
    }

	@Override
	protected void reinitiateText() {
		//Nothing
	}

	@Override
	public void handleClickData(ClickData data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleMouseMoveData(Location mData) {
		// TODO Auto-generated method stub
		
	}
    
}
