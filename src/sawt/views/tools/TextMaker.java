package sawt.views.tools;

import entities.views.children.Text;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.EntityCalculator;
import utilities.interfaces.IOperator;

public class TextMaker {
	
	private Text text;
	private String fontName;
	private String fontColor;
	private int size;
	
	/**
	 * 
	 * @param text
	 * @param fontName
	 * @param fontColor
	 * @param size
	 */
	public TextMaker(String text, String fontName, String fontColor, int size) {
		this.size = size; this.fontName = fontName; this.fontColor = fontColor;
		
		this.text = this.getText(text);
	}
	
	public Text getText() {
		return this.text;
	}
	
	public Text getText(String text) {
		Dimensions dim = new Dimensions(EntityCalculator.getBoundsOfText(text, fontName, size + "")[0] + "",  
				fontName, size + "", fontColor);
		return new Text(null, text, dim, new Location(0, 0));
	}


}
