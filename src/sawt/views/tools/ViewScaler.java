package sawt.views.tools;

import entities.views.ViewerEntity;

public class ViewScaler {
	
	private int DESIRED_ENTITY_WIDTH;
	private int DESIRED_ENTITY_HEIGHT;
	
	public ViewScaler(int desiredWidth, int desiredHeight) {
		this.DESIRED_ENTITY_WIDTH = desiredWidth;
		this.DESIRED_ENTITY_HEIGHT = desiredHeight;
	}

	/**
	 * Scale stack item image to fit into desired width and height.
	 * @param childView
	 */
	public void scaleChildView(ViewerEntity childView) {
		float decimalX = ( DESIRED_ENTITY_WIDTH / 
				((float)childView.getDimensions().getWidth()) );
		if(decimalX < 1) childView.scale(decimalX);
		float decimalY = ( DESIRED_ENTITY_HEIGHT / 
				((float)childView.getDimensions().getHeight()) );
		if(decimalY < 1) childView.scale(decimalY);
	}

}
