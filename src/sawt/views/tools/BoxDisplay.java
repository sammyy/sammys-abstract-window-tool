package sawt.views.tools;

import java.util.List;

import entities.views.ViewerEntity;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;

/**
 * Used to fit ViewerEntity objects inside an area.
 * With a desired maximum size for each object.
 * @author Sammyy
 *
 */
public class BoxDisplay {
	
	
	private int entityNumber = 0;

	private int X_START;
	private int Y_START;
	private int ROW_SPACE;
	private int ENTITY_SPACE;
	private int DESIRED_ENTITY_WIDTH;
	private int DESIRED_ENTITY_HEIGHT;
	
	//Area where each stacks is placed into.
	private int WIDTH_BOX_AREA;
	private int HEIGHT_BOX_AREA;
	
	/**
	 * 
	 * @param boxArea
	 * @param startAt
	 * @param rowSpace Space betwen rows of entities.
	 * @param entitySpace Space between each entity.
	 * @param desiredEntityWidth
	 * @param desiredEntityHeight
	 */
	public BoxDisplay(Dimensions boxArea,
			Location startAt, int rowSpace, int entitySpace, 
			int desiredEntityWidth, int desiredEntityHeight) {
		this.X_START = startAt.getX();
		this.Y_START = startAt.getY();
		
		this.ROW_SPACE = rowSpace;
		this.ENTITY_SPACE = entitySpace;
		
		this.DESIRED_ENTITY_WIDTH = desiredEntityWidth;
		this.DESIRED_ENTITY_HEIGHT = desiredEntityHeight;
		
		this.WIDTH_BOX_AREA = boxArea.getWidth();
		this.HEIGHT_BOX_AREA = boxArea.getHeight();
	}
	
	/**
	 * Reset the entity count to start placing
	 * entities from the start position again.
	 */
	public void resetEntityCount() {
		this.entityNumber = 0;
	}
	
	/**
	 * Scale ViewerEntity to fit inside the box and
	 * place its location relative to the boxarea.
	 */
	public void setLocationFor(ViewerEntity entityInsideBoxArea) {
		int x = this.xPositionForChild(entityNumber);
		int y = this.yPositionForChild(entityNumber);
		entityNumber++;
		Location loc = new Location(x, y);
		this.scaleChildView(entityInsideBoxArea);
		entityInsideBoxArea.setLocation(loc);
		
	}
	
	/**
	 * Maximum amount of rows of entities that will fit into the box area.
	 * @return
	 */
	public int getMaxRows() {
		double heightPerRow = (double) (DESIRED_ENTITY_HEIGHT + ROW_SPACE);
		double maxRows = this.HEIGHT_BOX_AREA / heightPerRow;
		return (int) maxRows;
	}
	
	/**
	 * Get the maximum amount of entities that will fit into
	 * the box area.
	 * @return
	 */
	public int getMaxEntities() {
		return this.getMaxRows() * this.totalIconsPerRow();
	}

	private int xPositionForChild(int child) {
		int column = child % this.totalIconsPerRow();
		return (X_START + 
				(column * (DESIRED_ENTITY_WIDTH + ENTITY_SPACE)) 
				);
	}
	
	/**
	 * Icons for stack items to fit per row.
	 * @return
	 */
	private int totalIconsPerRow() {
		int iconsPerRow = WIDTH_BOX_AREA / (DESIRED_ENTITY_WIDTH + ENTITY_SPACE);
		return iconsPerRow;
	}
	
	private int yPositionForChild(int child) {
		int row = child / this.totalIconsPerRow();
		return (Y_START + ((DESIRED_ENTITY_HEIGHT + ROW_SPACE) * row));
	}
	
	/**
	 * Scale stack item image to fit into desired width and height.
	 * @param childView
	 */
	private void scaleChildView(ViewerEntity childView) {
		float decimalX = ( DESIRED_ENTITY_WIDTH / 
				((float)childView.getDimensions().getWidth()) );
		if(decimalX < 1) childView.scale(decimalX);
		float decimalY = ( DESIRED_ENTITY_HEIGHT / 
				((float)childView.getDimensions().getHeight()) );
		if(decimalY < 1) childView.scale(decimalY);
	}

}
