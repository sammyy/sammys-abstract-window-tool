package sawt.views.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Paginate through a list of T.
 * @author Sammyy
 *
 * @param <T>
 */
public class Paginable<T> {
	
	private Collection<T> entities;
	private int entitiesPerPage;
	private Integer page = 0;
	
	public Paginable(Collection<T> entities, int entitiesPerPage) {
		this.entities = entities;
		this.entitiesPerPage = entitiesPerPage;
	}
	
	/**
	 * Get the entities to be shown on the given page number
	 * @param pageNumber
	 * @return
	 */
	private List<T> getEntitiesFor(int pageNumber){
		if(pageNumber > this.getTotalPages()) {
			throw new IllegalArgumentException("Exceeding page count!");
		}
		
		int startIndex = pageNumber * this.entitiesPerPage;
		//Non-inclusive endindex
		int endIndex = startIndex + this.entitiesPerPage;
		if(endIndex > this.entities.size()) endIndex = this.entities.size();
		
		List<T> entityList = new ArrayList<>(entities);
		
		return entityList.subList(startIndex, endIndex);
	}
	
	/**
	 * Get the entities to be shown on the given page number
	 * @param pageNumber
	 * @return
	 */
	public List<T> getCurrentPage(){
		return this.getEntitiesFor(this.page);
	}
	
	/**
	 * Total available pages of entities
	 * @return
	 */
	public int getTotalPages() {
		int entitiesTotal = this.entities.size();
		float pages = entitiesTotal / ((float) entitiesPerPage);
		return (int) Math.ceil(pages);
	}
	
	public int getPageNumber() {
		return this.page;
	}
	
	public void setPage(int pageNumber) {
		if(pageNumber > this.getTotalPages()) {
			throw new IllegalArgumentException("Exceeding page count!");
		}
		this.page = pageNumber;
	}

}
