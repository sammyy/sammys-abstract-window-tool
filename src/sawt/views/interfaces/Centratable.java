package sawt.views.interfaces;

import entities.views.ViewerEntity;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;
import utilities.interfaces.IOperator;

/**
 * Contract for being a ViewerEntity which
 * can be centrated.
 * @author Sammyy
 *
 */
public interface Centratable {
	

	default void centrate(IOperator operator, ViewerEntity view, Dimensions window) {
		if(operator== null) return;
		
		Dimensions dim = window;
		if(dim == null)  return;
		
		int WIDTH = view.getDimensions().getWidth();
		int HEIGHT = view.getDimensions().getHeight();
		
		int x = (dim.getWidth() - WIDTH) / 2;
		int y = (dim.getHeight() - HEIGHT) / 2;
		Location loc = new Location(x, y);
		view.setLocation(loc);
	}
	
}
