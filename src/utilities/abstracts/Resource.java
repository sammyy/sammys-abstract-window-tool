/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.abstracts;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to store data.
 * @author Sammyy
 */
public abstract class Resource<T> {
	
    private Map<String, T> properties = new HashMap<String, T>();
    
    public Resource(){
    
    }
    
    public Resource(Map<String, T> properties){
        properties.putAll(properties);
    }
    
    protected void addProperty(String key, T prop){
        properties.put(key, prop);
    }
    /**
     * 
     * @param key
     * @return null|T
     */
    protected T getProperty(String key){
        try{
        return properties.get(key);
        } catch (NullPointerException e){
            return null;
        }
    }
    
    protected Map<String, T> getProperties(){
        return Collections.unmodifiableMap(properties);
    }
    
    protected void clearProperties(){
        properties.clear();
    }
    
}
