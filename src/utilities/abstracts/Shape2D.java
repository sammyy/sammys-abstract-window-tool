/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.abstracts;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import entities.foundation.Ellips;
import entities.foundation.Entity;
import entities.foundation.Line;
import entities.foundation.Rectangle;
import resources.data.containers.Location;
import resources.exceptions.MultipleIntersectionsPointsForLineException;


/**
 *
 * @author Sammyy
 * Observe that setLocation and location behave differently in Shape2D from Entity
 */
public abstract class Shape2D extends Entity{
    
    private Location a = null;
    private Location b = null;

    public Shape2D(String data) {
        super(data);
    }
    
    public abstract void initAll(Location a, Location b);
    
    public Location getA() {
        return (a == null) ? null : a.getCopy();
    }

    public void setA(Location a) {
        this.a = a;
    }

    public Location getB() {
        return (b == null) ? null : b.getCopy();
    }

    public void setB(Location b) {
        this.b = b;
    }
    
    public abstract boolean isInside(int x, int y);
    
    public int getHeight(){
        if(this.a != null && this.b != null){
            return Math.abs(this.a.getY()-this.b.getY());
        }
        return 0;
    }
    
    public int getWidth(){
        if(this.a != null && this.b != null){
            return Math.abs(this.a.getX()-this.b.getX());
        }
        return 0;
    }

    @Override
    public void scale(float decimal){
        //this.scalePosition(decimal);//Make position become relative to scaled Shape
        
        Location a = this.a.getCopy();
        Location b = this.b.getCopy();
        //Location.scaleDifference(decimal, a, b);
        a.multiply(decimal);
        b.multiply(decimal);
        try {
            this.initAll(a, b);
        } catch (Exception ex) {
            Logger.getLogger(Shape2D.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
     /**
     * Scales the position of the Shape. Used to keep relative position to
     * parent entity(if has one).
     * @param collision
     * @param scaleDecimal 
     */
    private void scalePosition(float scaleDecimal) {
        Location a = this.getA();
        Location b = this.getB();
        Location.multiplyRelativity(scaleDecimal, a, b);
        this.setA(a);
        this.setB(b);
    }
    
     /**
     * Used by parent to move this shape.
     * @param loc
     */
    @Override
    public void setLocation(Location loc){
        super.setLocation(loc);
    }
    
    public Shape2D getShapeNonRelative() {
    	Shape2D s = this;
    	if(s instanceof Ellips) {
    		return new Ellips(getA().add(this.getLocation()), getB().add(this.getLocation()));
    	} else if(s instanceof Line) {
    		Line l = new Line(getA().add(
    				this.getLocation()), getB().add(this.getLocation()), 
    				((Line) s).getCollisionBelow());
    		for(Entity e: this.getChildEntities()) {
    			if(e instanceof Line) {
    				Line l2 = (Line) e;
    				l2 = new Line(l2.getA().add(
    	    				this.getLocation()), l2.getB().add(this.getLocation()), 
    	    				((Line) l2).getCollisionBelow());
    				l.addAdditionalLine(l2);
    			}
    		}
    		return l;
    	} else if(s instanceof Rectangle) {
    		return new Rectangle(getA().add(
    				this.getLocation()), getB().add(this.getLocation())
    				);
    	}
    	return null;
    }
    
    public abstract List<Location> intersectsWith(Shape2D shape) throws MultipleIntersectionsPointsForLineException;
    
}
