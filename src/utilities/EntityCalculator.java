/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import utilities.handlers.HandlersContainer;
import entities.foundation.Entity;
import entities.views.ViewerEntity;
import entities.views.children.Text;
import resources.data.containers.Dimensions;
import resources.data.containers.Location;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;

import utilities.interfaces.Childable;

/**
 * Used for calculating things with entities in the game.
 * Eg: centering positions for entities, keeping entities withing bounds &
 * collision calculation.
 * @author Sammyy
 */
public class EntityCalculator {
	
    /**
     * Returns a list where to place every entity to get them all in the
     * center with calculated spaces between each two objects. List indices of
     * returned locations corresponds to given list.
     * @param ents
     * @param spaceBetween pixels
     * @param containerWidth wherein to centrate
     * @param containerHeight wherein to centrate
     * @param xOffset adds this amount to every location, pixels
     * @param yOffset adds this amount to every location, pixels
     * @return 
     */
    public static List<Location> getCentratedAlignedRow(List<Entity> ents, 
            int spaceBetween, int containerWidth, int containerHeight,
            int xOffset, int yOffset){
        List<Location> locs = new ArrayList<>();
        
        int entitiesWidth = EntityCalculator.getTotalWidth(ents);
        int spaceBetweenTotal = spaceBetween*(ents.size()-1);
        int highestEntity = EntityCalculator.getHighestEntityHeight(ents);
        
        int x = (containerWidth - entitiesWidth - spaceBetweenTotal)/2;
        int y = (containerHeight - highestEntity)/2;
        x += xOffset; y += yOffset;
        
        int currentXPos = x;
        Location loc;
        for(Entity e: ents){
            loc = new Location(currentXPos,y);
            currentXPos += e.getDimensions().getWidth()
                    +spaceBetween;
            locs.add(loc);
        }
        
        return locs;
    }
    
     public static List<Location> getCentratedAlignedRow(List<Entity> ents, 
            int spaceBetween, Entity container){
        return EntityCalculator.getCentratedAlignedRow(ents, spaceBetween, 
                 container.getDimensions().getWidth(), 
                 container.getDimensions().getHeight(),
                 0, 0);
     }
    
    /**
     * Returns a list where to place every entity to get them all in the
     * center with calculated spaces between each two objects. List indices of
     * returned locations corresponds to given list.
     * @param ents
     * @param spaceBetween pixels
     * @param containerWidth wherein to centrate
     * @param containerHeight wherein to centrate
     * @param xOffset adds this amount to every location, pixels
     * @param yOffset adds this amount to every location, pixels
     * @return 
     */
    public static List<Location> getCentratedAlignedColumn(List<Entity> ents, 
            int spaceBetween, int containerWidth, int containerHeight,
            int xOffset, int yOffset){
        List<Location> locs = new ArrayList<>();
        
        int entitiesHeight = EntityCalculator.getTotalHeight(ents);
        int spaceBetweenTotal = spaceBetween*(ents.size()-1);
        int widestEntity = EntityCalculator.getWidestEntityWidth(ents);
        
        int y = (containerHeight - entitiesHeight - spaceBetweenTotal)/2;
        int x = (containerWidth - widestEntity)/2;
        x += xOffset; y += yOffset;
        
        int currentYPos = y;
        Location loc;
        for(Entity e: ents){
            loc = new Location(x, currentYPos);
            currentYPos += Integer.valueOf(e.getDimensions().getHeight())
                    +spaceBetween;
            locs.add(loc);
        }
        
        return locs;
    }
    
    /**
     * Returns a list of location for each entity so that they are placed at their
     * own row and each row is to the left.
     * @param ents
     * @param spaceBetween pixels
     * @param xOffset adds this amount to every location, pixels
     * @param yOffset adds this amount to every location, pixels
     * @return 
     */
    public static List<Location> getLeftAlignedRows(List<Entity> ents, 
            int spaceBetween, int xOffset, int yOffset){
        List<Location> locs = new ArrayList<>();
        
        int y = 0;
        int x = 0;
        x += xOffset; y += yOffset;
        
        int currentYPos = y;
        Location loc;
        for(Entity e: ents){
            loc = new Location(x, currentYPos);
            currentYPos += Integer.valueOf(e.getDimensions().getHeight())
                    +spaceBetween;
            locs.add(loc);
        }
        
        return locs;
    }
    
     public static List<Location> getCentratedAlignedColumn(List<Entity> ents, 
            int spaceBetween, Entity container){
    	 if(container.getDimensions() == null) return new ArrayList<>();
        List<Location> locs = EntityCalculator.getCentratedAlignedColumn(ents, spaceBetween, 
                 container.getDimensions().getWidth(), 
                 container.getDimensions().getHeight(),
                 0, 0);
        return locs;
     }
    
    /**
     * Total width of all entities.
     * @param ents
     * @return 
     */
    public static int getTotalWidth(List<Entity> ents){
        int total = 0;
        for(Entity e: ents){
            total += e.getDimensions().getWidth();
        }
        return total;
    }
    
    public static int getHighestEntityHeight(List<Entity> ents){
       int highest = 0;int current;
        for(Entity e: ents){
            current = e.getDimensions().getHeight();
            if(current > highest){
                highest = current;
            }
        }
        return highest; 
    }
    
      /**
     * Total height of all entities.
     * @param ents
     * @return 
     */
    public static int getTotalHeight(List<Entity> ents){
        int total = 0; Integer current;
        for(Entity e: ents){
            current = (e.getDimensions().getHeight() == null) ?
                      Integer.valueOf(e.getDimensions().getFontSize()) :
                    Integer.valueOf(e.getDimensions().getHeight());
            total += current;
        }
        return total;
    }
    
    public static int getWidestEntityWidth(List<? extends Entity> ents){
       int widest = 0;int current;
        for(Entity e: ents){
            current = Integer.valueOf(e.getDimensions().getWidth());
            if(current > widest){
                widest = current;
            }
        }
        return widest; 
    }
    
    /**
     * Pairs location with entities through their indices & sets
     * entities locations.
     * @param locs
     * @param ents 
     */
    public static void setLocations(List<Location> locs, List<Entity> ents){
        int count = 0;
        Entity e;
        for(Location loc: locs){
            e = ents.get(count);
            if(e instanceof Childable){
                ((Childable) e).setLocation(loc);
            } else if (e instanceof ViewerEntity){
                ((ViewerEntity) e).setLocation(loc);
            }
            count++;
        }
    }
    
    /**
     * Tells whether the area an entity is covering is clicked.
     * Doesn't account for any other entities in same location.
     * @param mouseClick
     * @param e
     * @return boolean
     */
    public static boolean isEntityAreaClicked(Location mouseClick, Entity e){
        if(mouseClick != null){
        	
        	Location eHeadLoc = e.getHeadLocation();
        	Dimensions eRealDimensions = e.getRealDimensions();

            int maxBoundX = (eHeadLoc.getX()+
                    Integer.valueOf(eRealDimensions.getWidth()));
            int maxBoundY = (eHeadLoc.getY()+
                    Integer.valueOf(eRealDimensions.getHeight()));
            
            if(mouseClick.getX() >= eHeadLoc.getX() &&
                    mouseClick.getX() <=  maxBoundX &&
                    mouseClick.getY() >= eHeadLoc.getY() &&
                    mouseClick.getY() <= maxBoundY){
                return true;
            }
        }
        return false;
    }
    
     /**
     * If an entity in the list has been clicked it will be returned.
     * Doesn't account for parent's children. If several entities are
     * clicked the one last in the list will be returned.
     * 
     * @param mouseClick
     * @param entities
     * @return Entity|null
     */
    public static Entity getClickedParentEntity(Location mouseClick, 
            Collection<Entity> entities){
        
        Entity clicked = null;
        
        for(Entity e: entities){
            if(EntityCalculator.isEntityAreaClicked(mouseClick, e)){
                clicked = e;
            }
        }
        
        return clicked;
    }
    /**
     * If an entity in the list has been clicked it will be returned, or if 
     * one of the entity's children was clicked they will be returned.
     * @param mouseClick
     * @param entities
     * @return Entity|null
     */
    public static Entity getClickedEntity(Location mouseClick, 
            Collection<Entity> entities){
        Entity e = EntityCalculator.getClickedEntity(mouseClick, entities, false);
        return e;
    }
    
    public static Entity getClickedEntity(Location mouseClick, 
            Collection<Entity> entities,
            boolean excludeTextEntities){
        Entity e = EntityCalculator.getClickedParentEntity(mouseClick, 
                entities);
        Entity child = null;
        if(e != null && !e.getChildEntities().isEmpty()){
            child = EntityCalculator.getClickedParentEntity(mouseClick, 
                    e.getChildEntities());
        }
        return ((child != null) ? 
                ((excludeTextEntities && child instanceof Text) ? e : child) : 
                e);
    }
    
    /**
     * Returns a list of Text entities, where each Text entity
     * is under the max limits and accounted for space between each row of text.
     * <br><br>
     * <b>OBS! This doesn't alter the locations of text elements. 
     * It only accounts for space for each element within the limits given.</b>
     * @param text
     * @param fontName
     * @param fontSize
     * @param maxWidth
     * @param maxHeight
     * @param handlers
     * @return 
     */
    public static List<Text> getTextEntitiesWithinBounds(String text,
            String fontName, String fontSize, String fontColor, int maxWidth, Integer maxHeight){
    	
        List<Text> texts = new ArrayList<>(); text += " ";//Additional space at end needed
        Dimensions textDimensions = new Dimensions(maxWidth, fontName, fontSize, fontColor);
        
        int heightOfText = EntityCalculator.getTextHeight(fontName, fontSize);

        String[] words = text.split(" ");
        StringBuilder rowBuilder = new StringBuilder();
        
        
        for(int i = 0; i < words.length; i++) {
        	
        	String spaceAtEnd = ((i+1 == words.length) ? "" : " ");
        	String word = words[i] + spaceAtEnd;//Word to add to a row
        	rowBuilder.append(word);//Add one word to row
        	
        	int width = EntityCalculator.getTextWidth(rowBuilder.toString(), fontName, fontSize);
        	
        	if(width >= maxWidth) {//Create a new Text element for completed row
        		
        		//Create a row by deleting overflowing word
        		int indexStart = rowBuilder.length() - word.length();
        		int indexEnd = indexStart + word.length();
        		rowBuilder.delete(indexStart, indexEnd); //Delete overflowing word
        		
        		String rowText = rowBuilder.toString(); //Create the row to add
        		
        		 //Start a new row with overflowing word
        		rowBuilder.setLength(0);
        		rowBuilder.append(word);
        		
        		Location loc = new Location(0 , 0);
        		Text textElement = new Text(null, rowText, textDimensions.getImmutableDimensions(), loc);
        		texts.add(textElement);
        		
        		
        		if((i + 1) == words.length) {//Last row of overflowing word(s)
            		
            		rowText = rowBuilder.toString(); //Create the row to add
            		
           		    loc = new Location(0 , 0);
           		    textElement = new Text(null, rowText, textDimensions.getImmutableDimensions(), loc);
           		    texts.add(textElement);
           		    
            	}
        		
        	} else if((i + 1) == words.length) {//Last row of overflowing word(s)
        		
        		String rowText = rowBuilder.toString(); //Create the row to add
       		    Location loc = new Location(0 , 0);
       		    Text textElement = new Text(null, rowText, textDimensions.getImmutableDimensions(), loc);
       		    texts.add(textElement);
       		    
        	}
        	
        	if(maxHeight != null && (texts.size() * heightOfText) > maxHeight) {
        		texts.remove(texts.size() - 1); //Delete overflowing Text row
        		break;
        	}
        }
     
        
        return texts;
    }
    
    /**
     * For testing purposes only.
     * @param args
     */
    public static void main(String[] args) {
    	/**
    	 * Test that everything prints within limits of boundaries.
    	 */
    	List<Text> texts = EntityCalculator.getTextEntitiesWithinBounds("ok ok2 ok4 ok5 ok ok ok5 ok ok ok ok ok ok shot", "Arial", "14", "#FFFFFF", 100);
    	for(int i = 0; i < texts.size(); i++) {
    		System.out.println(texts.get(i).getData() + ":" + EntityCalculator.getTextWidth(texts.get(i).getData(), "Arial", "14"));
    	}
    }

    /**
     * Returns a list of Text entities, where each Text entity
     * is within max limits. Rows of text.
     * 
     * {@linkplain #getTextEntitiesWithinBounds(String, String, String, String, int, Integer)}
     * @param text
     * @param fontName
     * @param fontSize
     * @param maxWidth
     * @param handlers
     * @return 
     */
    public static List<Text> getTextEntitiesWithinBounds(String text,
            String fontName, String fontSize, String fontColor, int maxWidth){
    	return EntityCalculator.getTextEntitiesWithinBounds(text, fontName, fontSize, fontColor, 
    			maxWidth, null);
    }
    
    /**
     * Get index in String which is closest to endOffset and a space character.
     * @param str String to find index of closest space.
     * @param endOffset Traverse characters backwards from this point. Exclusive
     * @return Index of closest space or null.
     */
    public static Integer getLastIndexForSpace(String str, int endOffset) {
    	int i = endOffset -1;
    	if(i < 0) return null;
    	while(!Character.isSpaceChar(str.charAt(i))) {
    		if(i > 0 ) {
    			i--;
    		} else {
    			return null;
    		}
    	}
    	return i;
    }
    
    /**
     * Returns an int array with first offset containing
     * the width of the given text, and second offset being
     * the height of the text. 
     * @param text
     * @param fontName
     * @param fontSize
     * @return 
     */
    public static int[] getBoundsOfText(String text,
            String fontName, String fontSize){
    	AffineTransform affinetransform = new AffineTransform();     
    	FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
    	Font f = new Font(fontName, Font.PLAIN, Integer.parseInt(fontSize));
        return new int[]{(int)(f.getStringBounds(text, frc).getWidth()), 
        		(int) f.getStringBounds(text, frc).getHeight()};
    }
    
    /**
     * Returns width of text
     * @param text
     * @param fontName
     * @param fontSize
     * @return Width
     */
    public static int getTextWidth(String text,
            String fontName, String fontSize){
        return getBoundsOfText(text, fontName, fontSize)[0];
    }
    
    /**
     * Returns height of font
     * @param fontName
     * @param fontSize
     * @return height
     */
    public static int getTextHeight(String fontName, String fontSize){
    	return getBoundsOfText("sS", fontName, fontSize)[1];
    }
    
    
    /**
     * Returns a font as accurately as possibly.
     * @param fontName
     * @param fontSize
     * @return Font
     */
    /*
    private static Font getAccurateFont(String fontName, String fontSize) {
    	switch(fontName) {
    	case "BlipBold": return Huppi.BLIP_BOLD.deriveFont(Float.valueOf(fontSize));
    	case "BlipNormal": return Huppi.BLIP_NORMAL.deriveFont(Float.valueOf(fontSize));
    	case "BlipItalic": return Huppi.BLIP_ITALIC.deriveFont(Float.valueOf(fontSize));
    	case "BlipBlack": return Huppi.BLIP_BLACK.deriveFont(Float.valueOf(fontSize));
    	default: return new Font(fontName, Font.PLAIN, Integer.valueOf(fontSize));
    	}
    }
    */
    
    private static JFrame getJFrame() {
    	return new JFrame();
    }
    /*
    public static void tryLoadCustomFonts() {
    	try {
    	     GraphicsEnvironment ge = 
    	         GraphicsEnvironment.getLocalGraphicsEnvironment();
    	     Font font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/huppi.ttf"));
    	     Huppi.BLIP_NORMAL = font;
    	     ge.registerFont(font);
    	     
    	     font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/huppi-italic.ttf"));
    	     Huppi.BLIP_ITALIC = font;
    	     ge.registerFont(font);
    	     
    	     font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/BlipBold.ttf"));
    	     Huppi.BLIP_BOLD = font;
    	     ge.registerFont(font);
    	     
    	     font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/huppi-large.ttf"));
    	     Huppi.BLIP_BLACK = font;
    	     ge.registerFont(font);

    	} catch (IOException|FontFormatException e) {
    	     //Handle exception
    		System.out.println(e.getStackTrace());
    	}
    } */
    
    /**
     * Used to get distance relative to an entity from another entity.
     * @param relativeTo The entity the other entity is relative to.
     * @param relative The entity that is the relative.
     * @return 
     */
    public static Location getRelativeDistance(Entity relativeTo, 
            Entity relative){
        return relative.getLocation().subtract(relativeTo.getLocation());
    }
    
      /**
     * Used to get location relative to an entity.
     * @param relativeTo The entity the other entity is relative to.
     * @param relative The location that is the relative.
     * @return 
     */
    public static Location getRelativePosition(Entity relativeTo, 
            Location relative){
        return relativeTo.getLocation().add(relative);
    }
    
	/**
	 * Given a list of entities and an Entity. An index for the Entity will be
	 * returned if it exists in the list.
	 * @param es
	 * @param current
	 * @return Index or null
	 */
	public static Integer getEntityIndex(List<? extends Entity> es, Entity current) {
		Integer counter = 0;
		for (Entity e : es) {
			if (current != null && e.getUniqueId().equals(current.getUniqueId())) {
				return counter;
			}
			counter++;
		}
		return null;
	}
    
    
}
