package utilities;

import resources.data.containers.Location;

public class MathAlgebra {
	
	/**
	 * Return whether one of the intervals (leftPoint, rightPint) 
	 * or (leftPointB, rightPointB) has an x value inside the other one.
	 * @param leftPoint
	 * @param rightPoint
	 * @param leftPointB
	 * @param rightPointB
	 * @return
	 */
	public static boolean xIntervalIntersects(Location leftPoint, 
			Location rightPoint, 
			Location leftPointB,
			Location rightPointB) {
		return MathAlgebra.xIntervalIntersects(leftPoint, rightPoint, leftPointB, rightPointB, false);
	}
	
	/**
	 * Return whether one of the intervals (leftPoint, rightPint) 
	 * or (leftPointB, rightPointB) has an x value inside the other one.
	 * @param leftPoint
	 * @param rightPoint
	 * @param leftPointB
	 * @param rightPointB
	 * @return
	 */
	public static boolean xIntervalIntersects(Location leftPoint, 
			Location rightPoint, 
			Location leftPointB,
			Location rightPointB, boolean recursive) {
		if(rightPointB.getX() <= rightPoint.getX() && rightPointB.getX() >= leftPoint.getX()) {
			return true;
		}
		if(leftPointB.getX() >= leftPoint.getX() && leftPointB.getX() <= rightPoint.getX()) {
			return true;
		}
		//If one value inside on of the intervals is inside the other interval.
		//Then there must be an edge(last or first x value) that is inside the other
		//Interval. There are three varitants: both edges of one interval inside
		//the other, or the left or right edge inside the other interval.
		if(!recursive) {
			return MathAlgebra.xIntervalIntersects(leftPointB, rightPointB, leftPoint, rightPoint, true);
		}
		return false;
	}
	
	/**
	 * Return whether one of the intervals (leftPoint, rightPint) 
	 * or (leftPointB, rightPointB) has an y value inside the other one.
	 * @param leftPoint
	 * @param rightPoint
	 * @param leftPointB
	 * @param rightPointB
	 * @return
	 */
	public static boolean yIntervalIntersects(Location leftPoint, 
			Location rightPoint, 
			Location leftPointB,
			Location rightPointB, boolean recursive) {
		if(rightPointB.getY() <= rightPoint.getY() && rightPointB.getY() >= leftPoint.getY()) {
			return true;
		}
		if(leftPointB.getY() >= leftPoint.getY() && leftPointB.getY() <= rightPoint.getY()) {
			return true;
		}
		if(!recursive) {
			return MathAlgebra.yIntervalIntersects(leftPointB, rightPointB, leftPoint, rightPoint, true);
		}
		return false;
	}
	
	/**
	 * true true true true false false
	 */
	public static void testXIntervalIntersects() {
		Location leftPoint = new Location(-2, 0);
		Location rightPoint = new Location(2, 0);
		Location leftPointB = new Location(-1, 0);
		Location rightPointB = new Location(1, 0);
		
		System.out.println(MathAlgebra.xIntervalIntersects(leftPoint, rightPoint, 
				leftPointB, rightPointB));
		System.out.println(MathAlgebra.xIntervalIntersects(leftPointB, rightPointB, 
				leftPoint, rightPoint));
		
		leftPointB = new Location(-4, 0);
		rightPointB = new Location(-2, 0);
		
		System.out.println(MathAlgebra.xIntervalIntersects(leftPoint, rightPoint, 
				leftPointB, rightPointB));
		System.out.println(MathAlgebra.xIntervalIntersects(leftPointB, rightPointB, 
				leftPoint, rightPoint));
		
		leftPointB = new Location(-4, 0);
		rightPointB = new Location(-3, 0);
		
		System.out.println(MathAlgebra.xIntervalIntersects(leftPoint, rightPoint, 
				leftPointB, rightPointB));
		System.out.println(MathAlgebra.xIntervalIntersects(leftPointB, rightPointB, 
				leftPoint, rightPoint));
	}

}
