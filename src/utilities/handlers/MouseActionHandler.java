/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.handlers;

import entities.foundation.Entity;
import entities.views.ViewerEntity;
import entities.views.children.Button;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import resources.data.containers.ClickData;
import resources.data.containers.Location;
import resources.dataHandlers.MouseHandler;
import utilities.EntityCalculator;
import utilities.interfaces.ClickListener;
import utilities.interfaces.Draggable;
import utilities.interfaces.IOperator;
import utilities.interfaces.MouseMoveListener;

/**
 * Used for handling focus of mouseHandler in 
 * game and doing actions using mouseHandler.
 * @author Sammyy
 */
public class MouseActionHandler {
    /**
     * Entities seen by the actual Player
     */
    private List<ViewerEntity> overlayEntities;
    
    private MouseHandler mHandler;
    private FocusHandler fHandler;
    
    private IOperator op;
    
    private ViewerEntity lastClicked = null;
    
    public MouseActionHandler(IOperator op, MouseHandler mHandler , FocusHandler focusHandler,
            LinkedList<ViewerEntity> overlayEntities){
    	this.op = op;
        this.mHandler = mHandler;
        this.fHandler = focusHandler;
        
        this.overlayEntities = overlayEntities;
    }
      
    public void printClickedName(){
        if(fHandler.getFocusedEntity() != null){
        System.out.println(fHandler.getFocusedEntity().getClass().getSimpleName()+
                " Id: "+fHandler.getFocusedEntity().getUniqueId());
        }
    }
    
    private List<Entity> wrapViewers(){
    	return new ArrayList<>(overlayEntities);
    }
    /**
     * Finds clicked ViewerEntity
     * @return 
     */
    private Entity getClickedEntity(){
            Entity e = EntityCalculator.
                    getClickedEntity(mHandler.getCurrentLocation(), 
                    		wrapViewers(), true);
            return e;
    }
    
    private Entity dragging = null;
    private Location dragDiff = null;
    private Entity getDraggedEntity(){
        Entity e = EntityCalculator.
                getClickedEntity(mHandler.getLastPreviousLocation(),
                		wrapViewers());
        if(e != null) dragDiff = e.getLocation().
        		subtract(mHandler.getLastPreviousLocation());
        return e;
    } 
    
    private void dragEntity(){
        Entity dragged = (dragging == null) 
        		? this.getDraggedEntity() : dragging;
        		
        if(dragged != null && dragged instanceof Draggable){
            Draggable e = (Draggable) dragged;
            Location moveTo = mHandler.getCurrentLocation().add(dragDiff);
            e.drag(this.op, moveTo);
        }
    }
    
    /**
     * If a button is clicked its click command is called.
     */
    private void clickButton(Entity e){
        if(e instanceof Button){
            ((Button) e).clickButton();
        }
    }
    
    /**
     * Called every time mouseHandler is updated
     * to do appropriate actions with the new input, such as
     * updating focuses.
     */
    public void updateActions(){
    	this.sendMouseMoveCallback();
        if(mHandler.isBeingDragged()){
            this.dragEntity();
        }
        
        if(mHandler.hasReleasedClick()){
            Entity e = this.getClickedEntity();
            this.lastClicked = (ViewerEntity) e;
            //Set focus for ViewerEntity clicked
            this.fHandler.setFocus((e == null) ? null : e);
                       
            this.sendClickCallback(e);
            this.clickButton(e);
            this.dragging = null;
        }
    }
    
    private ClickData getClickData(Entity clicked) {
    	Boolean leftClick = this.mHandler.getLeftMouseClick();
    	ClickData cd = new ClickData(this.mHandler.getCurrentLocation(), clicked, 
        		leftClick);
    	return cd;
    }
    
    @SuppressWarnings("unchecked")
	private void sendClickCallback(Entity clicked){
    	List<ClickListener<ClickData>> cls = new ArrayList<>(this.overlayEntities);
    	
    	if(clicked instanceof ClickListener) {//Click listener for clicked entity
        	cls.add((ClickListener<ClickData>) clicked);
    	}
    	
        ClickData cd = this.getClickData(clicked);
        for(ClickListener<ClickData> cl: cls){
            cl.handleClickData(cd);
        }
    }
    
    private void sendMouseMoveCallback() {
    	List<MouseMoveListener<Location>> cls = new ArrayList<>(this.overlayEntities);
        Location loc = this.mHandler.getCurrentLocation();
        for(MouseMoveListener<Location> cl: cls){
            cl.handleMouseMoveData(loc);
        }
    }
   
    
}
