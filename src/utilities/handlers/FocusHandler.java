/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.handlers;

import entities.foundation.Entity;
import entities.views.InputEntity;

/**
 * Used for controlling focused entity.
 * @author Sammyy
 */
public class FocusHandler {
    
        
    private Entity focusedEntity;
    
    public FocusHandler(){
        focusedEntity = null;
    }
  
    public void setFocus(Entity e){
        this.focusedEntity = e;
        if(focusedEntity == null || focusedEntity instanceof InputEntity){
        	if(focusedEntity == null) {
        		focusedEntity = null;
        	} else {
                ((InputEntity) focusedEntity).setFocused();
        	}
        }
    }

    public Entity getFocusedEntity() {
        return focusedEntity;
    }
    
}
