/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.handlers;

import resources.dataHandlers.KeyInputHandler;
import resources.dataHandlers.MouseHandler;
import utilities.abstracts.Resource;

/**
 * Used for containing handlers being sent to a
 * constructor for dependeny injection.
 * @author Sammyy
 */
public class HandlersContainer extends Resource<Object>{

    public static final String MOUSE_HANDLER = "mouseHandler";
    public static final String KEY_HANDLER = "keyHandler";

    public HandlersContainer(MouseHandler mHandler, KeyInputHandler kHandler){
        super.addProperty(MOUSE_HANDLER, mHandler);
        super.addProperty(KEY_HANDLER, kHandler);
    }
   
    public MouseHandler getMouseHandler(){
        return (MouseHandler)this.getProperty(MOUSE_HANDLER);
    }
    
    public KeyInputHandler getKeyInputHandler(){
        return (KeyInputHandler)this.getProperty(KEY_HANDLER);
    }
    
    public HandlersContainer getControllerHandlers(){
        return new HandlersContainer(this.getMouseHandler(), 
                this.getKeyInputHandler());
    }
    
}
