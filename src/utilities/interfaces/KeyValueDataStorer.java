/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

import java.io.File;

/**
 * T type used for storing. T2 is the id used for getting
 * stored T type.
 * @author Sammyy
 */
public interface KeyValueDataStorer<T, T2> {
    public boolean store(T store);
    public T getStored(T2 id);
}
