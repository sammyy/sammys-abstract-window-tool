/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;


/**
 *
 * @author Sammyy
 */
public interface Collidable<T> {
    public boolean hasCollided(T collidedWith);
}
