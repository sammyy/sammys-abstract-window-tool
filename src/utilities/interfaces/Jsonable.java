/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

/**
 * Used to indicate an object can be put into
 * json format
 * @author Sammyy
 */
public interface Jsonable {
    public String getAsJson();
}
