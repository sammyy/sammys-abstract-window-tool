/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

import resources.data.containers.Location;

/**
 * Interface to make something move.
 * @author Sammyy
 */
public interface Movable<T> {
    public void move(T location);
    
}
