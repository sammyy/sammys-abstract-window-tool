/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

import resources.data.containers.Location;

/**
 * With help of this parents cant change locations of children
 * @author Sammyy
 */
public interface Childable {
    public void setLocation(Location loc);
}
