package utilities.interfaces;

public interface I2DSize {
	int getWidth();
	int getHeight();
}
