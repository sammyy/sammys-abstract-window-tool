/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

import resources.data.containers.Location;

/**
 * An object that listens for clickdata.
 * @author Sammyy
 */
public interface ClickListener<T> {
    public void handleClickData(T data);
}
