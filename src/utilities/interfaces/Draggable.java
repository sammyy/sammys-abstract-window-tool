/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

import resources.data.containers.Location;

/**
 * Makes something draggable
 * @author Sammyy
 */
public interface Draggable {
    public void drag(IOperator op, Location loc);
}
