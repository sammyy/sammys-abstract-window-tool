package utilities.interfaces;

import entities.views.ViewerEntity;
import resources.data.containers.UpdateStatus;
/**
 * An operator of ViewerEntitys which can be shown or not shown.
 * @author Sammyy
 *
 */
public interface IOperator {
	public void addOverlayUpdate(ViewerEntity e, UpdateStatus status);
	public boolean removeOverlayEntity(ViewerEntity e);
	public boolean hasOverlayEntity(ViewerEntity e);
}
