package utilities.interfaces;

public interface Retrievable<T> {
	/**
	 * Retrieve T
	 * @return
	 */
	public T retrieve();
}
