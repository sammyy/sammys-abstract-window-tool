package utilities.interfaces;

public interface KeyInputListener {
	
	public void onKeyInput(String input);

}
