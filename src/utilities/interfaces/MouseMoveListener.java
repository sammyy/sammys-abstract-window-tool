package utilities.interfaces;

public interface MouseMoveListener<T> {
	public void handleMouseMoveData(T mData);
}
