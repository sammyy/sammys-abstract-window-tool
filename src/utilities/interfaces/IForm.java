/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.interfaces;

/**
 * Every form should contain a button, so
 * it should also have a clickButton method.
 * @author Sammyy
 */
public interface IForm {
    public void onButtonClick(String buttonId);
}
